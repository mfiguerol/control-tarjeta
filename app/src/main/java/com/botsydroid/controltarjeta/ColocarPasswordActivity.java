package com.botsydroid.controltarjeta;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ColocarPasswordActivity extends AppCompatActivity {
    EditText password1;
    Button aceptar;
    String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colocar_password);
        SharedPreferences settings=getSharedPreferences("PREFS",0);
        password=settings.getString("password_mallku","");
        final EditText pass1 = (EditText) findViewById(R.id.password1);
        Button boton = (Button) findViewById(R.id.Aceptar);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text1,text2;
                text1=pass1.getText().toString();
                  if(text1.equals(password))
                    {
                       Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                       startActivity(intent);
                       finish();
                    }
                    else
                    {
                        Toast.makeText(ColocarPasswordActivity.this,"Password Incorrecto", Toast.LENGTH_SHORT).show();
                    }
                }


        });
    }
}
