package com.botsydroid.controltarjeta;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.*;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.Manifest;

import java.util.Calendar;
import java.util.Date;

import android.app.AlarmManager;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;


public class MainActivity extends AppCompatActivity
        implements Principal.OnFragmentInteractionListener, Contactenos_Fragment.OnFragmentInteractionListener,
        FragmentAlert.OnFragmentInteractionListener, ListFragment.OnFragmentInteractionListener,
        StatusFragment.OnFragmentInteractionListener, NavigationView.OnNavigationItemSelectedListener,
        ConfigFragment.OnFragmentInteractionListener, SettingFragment.OnFragmentInteractionListener,SettingAlertasFragment.OnFragmentInteractionListener

{
    TextView titulo;
    String numero_disp;
    public boolean GeoFence_activo;


    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public

    String clave;
    String desc_disp;
    Cursor punto_para_zona = null;


    public float radio_zona;
    private static String SENT = "SMS_SENT";
    private static String DELIVERED = "SMS_DELIVERED";
    private static int MAX_SMS_MESSAGE_LENGTH = 160;
    private static int timeHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
    private static int timeMinute = Calendar.getInstance().get(Calendar.MINUTE);
    private static MainActivity inst;
    private static final int MY_PERMISSION_RESPONSE = 2;
    private View mProgressView;
    private View mLoginFormView;
    private String encendido;
    private String lock;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private Intent alarmIntent;
    private TextView alarmTextView;
    BroadcastReceiver broadcastReceiver;
    BroadcastReceiver myReceiverDev;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        alarmTextView = (TextView) findViewById(R.id.alarmTextView);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.cintillo_solo_letras_tope);
        if(encendido==null)
        encendido="";
        if(lock==null)
        lock="";
        setSupportActionBar(toolbar);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //alarmIntent= new Intent(MainActivity.this,receptor.class);
        //AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.bringToFront();
        navigationView.requestLayout();
        SharedPreferences prefe = getSharedPreferences("Numero", Context.MODE_PRIVATE);
        numero_disp = prefe.getString("num", "nada");
        desc_disp = prefe.getString("desc", "nada");
        clave = prefe.getString("clave", "101010");
        if (numero_disp.equals("nada")) {
            Agregar_numero();
        } else {
            Colocar_numero(desc_disp, numero_disp);
           // logo.setImageResource(R.drawable.cintillo_solo_letras);
        }
        mLoginFormView = findViewById(R.id.Contenedor);
        mProgressView = findViewById(R.id.login_progress);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
/*        pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0,
                alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);*/
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                //Cry about not being clicked on
            } else if (extras.getBoolean("NotiClick")) {
                Bundle args = new Bundle();
                args.putString("mensaje", extras.getString("men"));
                args.putString("numero", extras.getString("numero"));
                Fragment fragment = FragmentAlert.newInstance("", "");
                fragment.setArguments(args);
                FragmentTransaction FT = getSupportFragmentManager().beginTransaction();
                FT.addToBackStack(null);
                FT.replace(R.id.Contenedor, fragment).commit();
                //Do your stuff here mate :)
            }

        }
        navigationView.setItemIconTintList(null);

        if (Build.VERSION.SDK_INT >= 23) {
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS)
                    != PackageManager.PERMISSION_GRANTED)) {
                Log.w("BleActivity", "Receive SMS access not granted!");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECEIVE_SMS},
                        MY_PERMISSION_RESPONSE);
            }
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
                    != PackageManager.PERMISSION_GRANTED)) {
                Log.w("BleActivity", "Receive SMS access not granted!");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.SEND_SMS},
                        MY_PERMISSION_RESPONSE);

            }
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)) {
                Log.w("BleActivity", "Location access not granted!");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSION_RESPONSE);
            }
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                    != PackageManager.PERMISSION_GRANTED)) {
                Log.w("BleActivity", "Location access not granted!");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSION_RESPONSE);
            }
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.WAKE_LOCK)
                    != PackageManager.PERMISSION_GRANTED)) {
                Log.w("BleActivity", "Location access not granted!");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WAKE_LOCK},
                        MY_PERMISSION_RESPONSE);
            }
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.VIBRATE)
                    != PackageManager.PERMISSION_GRANTED)) {
                Log.w("BleActivity", "Location access not granted!");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.VIBRATE},
                        MY_PERMISSION_RESPONSE);
            }
        }


    }



    public void Seleccionar_dispositivo() {
        final Cursor auxTodos;
        MessagesDb auxR = new MessagesDb(getBaseContext());
        auxTodos = auxR.datos();
    /*String a;
     String b;*/
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Seleccione");
        builder.
                setSingleChoiceItems(auxTodos, 1, "nombre", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        auxTodos.moveToPosition(item);
                        numero_disp = auxTodos.getString(1);
                        desc_disp = auxTodos.getString(2);
                        TextView nombre = (TextView) findViewById(R.id.tvNombreTelefono);
                        nombre.setText("MallKu"+desc_disp);


                    }
                })
                .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Colocar_numero(desc_disp, numero_disp);
                        SharedPreferences prefs = getSharedPreferences("Numero", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.clear();
                        editor.putString("num", numero_disp);
                        editor.putString("desc", desc_disp);

                        editor.commit();



                    }

                })
        ;
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void Colocar_numero(String a, String b) {
        View hView = navigationView.getHeaderView(0);
        TextView tvNombreTelefono = (TextView) hView.findViewById(R.id.tvNombreTelefono);
        tvNombreTelefono.setText(a);
        getSupportActionBar().setTitle(a);
        colocar_fondo("");
        Intent in = new Intent("my.action.string");
        in.putExtra("state", numero_disp);
        sendBroadcast(in);
        titulo = (TextView) findViewById(R.id.tvNombreTelefono);
        //titulo.setText(b+" - "+a);
    }

    private boolean buscar_punto_zona() {
        Cursor pun;
        boolean regresa = true;
        MessagesDb todos = new MessagesDb(getBaseContext());
        pun = todos.punto_zona();
        if (pun != null) {
            pun.moveToFirst();
            do {
                int cual = pun.getInt(0);

                todos.modificar_punto(radio_zona, cual);

            } while (pun.moveToNext());
        } else
            regresa = false;
        return regresa;
    }

    public void Agregar_punto_mapa(String tipo, String texto, String numero) {
        double lat = 0, lon = 0;
        Date fechita = null;
        //Toast.makeText(this, " Cadena Recibida"+texto, Toast.LENGTH_LONG).show();
        String[] varios = texto.split("\n");
        if (varios[0].contains("lon:")){
            String[] parte=varios[0].split(" ");
            try {
                String[] valores = parte[0].split(":");
                Log.i("Punto", "lat" + valores[1]);

                lat = Double.parseDouble(valores[1]);
                String[] valores2 = parte[1].split(":");
                Log.i("Punto", "lon" + valores2[1]);

                lon = Double.parseDouble(valores2[1]);

            }
            catch (Exception e)
            {
                lat=0;
                lon=0;
            }
            MessagesDb R1 = new MessagesDb(getBaseContext());
            ContentValues values = new ContentValues();
            values.put("numero", numero);
            values.put("longitud", lon);
            values.put("latitud", lat);
            values.put("tipo", "M");
            values.put("enviado", varios[3].substring(2));
            R1.insertar_punto(values);
            Log.i("Punto", "punto" + values.toString());
            R1.close();
            String[] speed = varios[1].split(":");
            //
            montar_mapa(speed[1], varios[4], varios[2]);
        }
        else {

            try {
                String[] valores = varios[0].split(":");
                Log.i("Punto", "lat" + valores[1]);
                lat = Double.parseDouble(valores[1]);
                String[] valores2 = varios[1].split(":");
                Log.i("Punto", "lon" + valores2[1]);
                lon = Double.parseDouble(valores2[1]);
            } catch (Exception e) {
                lat = 0;
                lon = 0;
            }
        Log.i("Fecha", "recibido" + varios[3]);
        MessagesDb R1 = new MessagesDb(getBaseContext());
        ContentValues values = new ContentValues();
        values.put("numero", numero);
        values.put("longitud", lon);
        values.put("latitud", lat);
        values.put("tipo", "M");
        values.put("enviado", varios[3].substring(2));
        R1.insertar_punto(values);
        Log.i("Punto", "punto" + values.toString());
        R1.close();
        String[] speed = varios[2].split(":");
        //Toast.makeText(this, " Cadena Recibida"+varios, Toast.LENGTH_LONG).show();
        montar_mapa(speed[1], varios[5], varios[3]);
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null)
            unregisterReceiver(broadcastReceiver);
        if (myReceiverDev != null)
            unregisterReceiver(myReceiverDev);
    }

    public void Agregar_punto_mapa(Bundle a) {
        double lat = 0, lon = 0, radio = 0;
        String tam, Texto, cual;
        String[] partes = new String[10];
        tam = (String) a.get("tam");//numero de telefono del que recibo
        Texto = (String) a.get("Texto");//para dividir las coordenadas
        cual = (String) a.get("tipo");//tipo mapa o la posicion en donde esta....
        partes = Texto.split(";");
        /*Toast.makeText(getBaseContext(), "tipo"+partes[0]+"parte= "+partes[1]+"parte"+partes[2],
                Toast.LENGTH_LONG).show();*/


        try {
            cual = partes[0];
            lat = Double.parseDouble(partes[1]);
            lon = Double.parseDouble(partes[2]);

        } catch (Exception e) {
            cual = "M";
            lat = 0;
            lon = 0;
            radio = 0;
        }
        MessagesDb R1 = new MessagesDb(getBaseContext());
        ContentValues values = new ContentValues();
        values.put("numero", tam);
        values.put("longitud", lon);
        values.put("latitud", lat);
        values.put("tipo", cual);
        R1.insertar_punto(values);
        Log.i("Punto", "punto" + values);
        R1.close();

        if (cual.equals("Z")) {
            definir_radio_zona();

        }

    }


    public void Agregar_numero() {
        LayoutInflater linf = LayoutInflater.from(this);
        final View inflator = linf.inflate(R.layout.agregar_numero, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Ingresa el numero");
        alert.setView(inflator);

        final EditText et1 = (EditText) inflator.findViewById(R.id.etDescripcion);
        final EditText et2 = (EditText) inflator.findViewById(R.id.etNumero);
        final EditText et3 = (EditText) inflator.findViewById(R.id.password_et_password);
        final EditText et4 =(EditText) inflator.findViewById(R.id.password_et_mallku);

        alert.setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String s1 = et1.getText().toString();
                String s2 = et2.getText().toString();
                String s3 = et3.getText().toString();
                String s4 = et4.getText().toString();

                //String aux=inputstreet.getText().toString();
                    /*Bundle args = new Bundle();
                    args.putString("nombre1", s1);
                    args.putString("nombre2", s2);*/
                SharedPreferences prefs = getSharedPreferences("Numero", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.putString("num", s2);
                editor.putString("desc", s1);
                editor.putString("clave", s3);


                editor.commit();
                MessagesDb R1 = new MessagesDb(getBaseContext());
                ContentValues values = new ContentValues();
                values.put("numero", s2);
                values.put("nombre", "" + s1);
                R1.insertar(values);
                R1.close();
                Colocar_numero(s1, s2);
                numero_disp = s2;
                clave = s3;

                //do operations using s1 and s2 here...
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });

        alert.show();
    }
    public void Activar_geofence()
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("¿Que deseas realizar?");
        alert.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                GeoFence_activo=true;
            }
        });

        alert.setNegativeButton("Desactivar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                GeoFence_activo=false;
                showProgress(true);
            }
        });

        alert.show();
    }
    public void Agregar_numero_ayuda() {
        LayoutInflater linf = LayoutInflater.from(this);
        final View inflator = linf.inflate(R.layout.agregar_numero_ayuda, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Ingresa el numero de contacto");
        alert.setView(inflator);

        final EditText et1 = (EditText) inflator.findViewById(R.id.etDescripcion);
        final EditText et2 = (EditText) inflator.findViewById(R.id.etNumero);


        alert.setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String s1 = et1.getText().toString();
                String s2 = et2.getText().toString();


                //revisar para guardar el numero de ayuda


                MessagesDb R1 = new MessagesDb(getBaseContext());
                ContentValues values = new ContentValues();
                values.put("numero", numero_disp);
                values.put("numero_ayuda", s2);
                values.put("descripcion", s1);
                R1.insertar_numero_ayuda(values);
                R1.close();


            }
        });

        alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });

        alert.show();
    }

    public void colocar_fondo(String accion) {
        Fragment fragment = Principal.newInstance("1", "2");
        Log.i("main", "entro prender el motor del colocar fondo: " + accion);
        Bundle args = new Bundle();
        args.putString("clave", clave);
        args.putString("numero_telefono",numero_disp);

        if (!accion.equals(""))
            args.putString("accion", accion);
        fragment.setArguments(args);
        FragmentTransaction FT = getSupportFragmentManager().beginTransaction();
        FT.addToBackStack(null);
        FT.replace(R.id.Contenedor, fragment).commit();
    }

    public void reset() {
        LayoutInflater linf = LayoutInflater.from(this);
        final View inflator = linf.inflate(R.layout.agregar_clave, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Que deseas realizar");
        alert.setPositiveButton("Resetear", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                enviar("reset" + clave);
                showProgress(true);
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });

        alert.show();

    }

    public void Cambiar_clave() {
        LayoutInflater linf = LayoutInflater.from(this);
        final View inflator = linf.inflate(R.layout.agregar_clave, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Ingresa la clave");
        alert.setView(inflator);

        final EditText et1 = (EditText) inflator.findViewById(R.id.change_password_et_password);

        alert.setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                enviar("password" + clave + " " + et1.getText().toString());
                clave = et1.getText().toString();
                //String aux=inputstreet.getText().toString();
                    /*Bundle args = new Bundle();
                    args.putString("nombre1", s1);
                    args.putString("nombre2", s2);*/
                SharedPreferences prefs = getSharedPreferences("Numero", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("clave", clave);
                editor.commit();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });

        alert.show();
    }

    public void sensibilidad(int cual) {

        enviar("sensitivity" + clave + " " + cual);
    }

    public void frecuencia(int frecuencia) {
        enviar("xtime" + clave + " " + String.format("%3s", frecuencia).replace(' ', '0'));


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            configuracion();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.Inicio) {
            // Handle the camera action
            fragemntoprincipal();

        }
        if (id == R.id.Nuevo_numero) {
            Agregar_numero();

        }

        if (id == R.id.setUbica) {
            enviar("Fix010s001n" + clave);
        }

        if (id == R.id.Status) {
            TraerStatus();


        }
        if (id == R.id.Seleccionar_numero) {
            Seleccionar_dispositivo();

        }
        if (id == R.id.mensaje_alarma) {


        }
       /*
        if(id ==R.id.nav_borrar)
        {
            borrar();
        }*/
        if (id == R.id.Contactenos) {
            mostar_contacto();
        }
        if (id == R.id.mensajes_de_alarma) {
            Log.i("Alarma", "Llama a la alarmas");
            mostar_alarmas();
        }
        if (id == R.id.mapa) {
            montar_mapa();
        }
        /* if(id==R.id.zona)
        {
            crear_zona();
        }*/
        if (id == R.id.mensajes_de_alarma) {
            listarMensajes();
        }
        if (id == R.id.nav_share) {
            configuracion();
        }
      /*  if(id==R.id.monitor)
        {

        }
*/
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void mostar_alarmas() {
       /* Fragment fragment =
        FragmentTransaction FT=getSupportFragmentManager().beginTransaction();
        FT.addToBackStack(null);
        FT.replace(R.id.Contenedor,fragment).commit();*/
    }


    private void mostar_contacto() {
        Fragment fragment = Contactenos_Fragment.newInstance("1", "1");
        FragmentTransaction FT = getSupportFragmentManager().beginTransaction();
        FT.addToBackStack(null);
        FT.replace(R.id.Contenedor, fragment).commit();
    }

    public void monitor() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Que deseas realizar");
        alert.setPositiveButton("Encender", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                enviar("monitor" + clave);
                showProgress(true);
            }
        });

        alert.setNegativeButton("Apagar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                enviar("tracker" + clave);
                showProgress(true);
            }
        });

        alert.show();
    }

    private void listarMensajes() {
        Fragment fragment = ListFragment.newInstance("numero_disp", "1");
        Bundle bundle = new Bundle();
        bundle.putString("numero_disp", numero_disp);
        fragment.setArguments(bundle);

        FragmentTransaction FT = getSupportFragmentManager().beginTransaction();
        FT.addToBackStack(null);
        FT.replace(R.id.Contenedor, fragment).commit();

    }

    private void configuracion() {
        // startActivity(new Intent(this, Configuracion.class));
        Fragment fragment = ConfigFragment.newInstance("1", "2");
        FragmentTransaction FT = getSupportFragmentManager().beginTransaction();
        FT.addToBackStack(null);
        FT.replace(R.id.Contenedor, fragment).commit();
    }

    public void configuracion_adicionales() {
        // startActivity(new Intent(this, Configuracion.class));
        Fragment fragment = SettingFragment.newInstance("1", "2");
        FragmentTransaction FT = getSupportFragmentManager().beginTransaction();
        FT.addToBackStack(null);
        FT.replace(R.id.Contenedor, fragment).commit();
    }
    public void configuracion_alertas()
    {
        Fragment fragment = SettingAlertasFragment.newInstance("1", "2");
        FragmentTransaction FT = getSupportFragmentManager().beginTransaction();
        FT.addToBackStack(null);
        FT.replace(R.id.Contenedor, fragment).commit();
    }

    private void montar_mapa(String speed, String varios, String fecha) {
        Intent mapita = new Intent(getBaseContext(), Mapas.class);
        SharedPreferences prefe = getSharedPreferences("Numero", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefe.edit();
        edit = edit.putString("radio", String.valueOf(radio_zona));
        edit = edit.putString("velocidad", speed);
        edit = edit.putString("varios", varios);
        edit = edit.putString("fecha", fecha);
        edit = edit.putString("clave", clave);
        edit.commit();
        /*Toast.makeText(getBaseContext(), "SharedPreferences"+edit.toString(),
                Toast.LENGTH_SHORT).show();*/
        startActivity(mapita);
    }



    private void montar_mapa() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Confirma")
                .setMessage("Desea Obtener la localización de " + desc_disp)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent mapita = new Intent(getBaseContext(), Mapas.class);
                        enviar("Fix010s001n" + clave);
                        //  startActivity(mapita);
                    }

                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }

                })
                .show();
    }

    public void definir_radio_zona() {
        radio_zona = 0;
        LayoutInflater linf = LayoutInflater.from(this);
        final View inflator = linf.inflate(R.layout.agregar_zona, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Ingresa el tamaño de la Zona");
        alert.setView(inflator);
        final EditText et1 = (EditText) inflator.findViewById(R.id.tam_zona);
        alert.setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String s1 = et1.getText().toString();
                radio_zona = Float.parseFloat(s1);

                if (radio_zona != 0) {
                    buscar_punto_zona();
                }

/*
                new CountDownTimer(60 * 1000, 30*1000) {//aqui se coloca cuanto dura el contador y cada cuanto opera.
                    @Override
                    public void onTick(long millisUntilFinished) {


                    }
                    @Override
                    public void onFinish() {

                    }
                };*/
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });

        alert.show();

    }

    private void fragemntoprincipal() {

        Fragment fragment = Principal.newInstance("1", "2");
        FragmentTransaction FT = getSupportFragmentManager().beginTransaction();
        FT.addToBackStack(null);
        FT.replace(R.id.Contenedor, fragment).commit();
    }

    private void TraerStatus() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Confirma")
                .setMessage("Desea Obtener el  status de " + desc_disp)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        enviar("Check" + clave);
                    }

                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }

                })
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("primero", "llego");
        Intent myIntent = getIntent();
        Bundle a = myIntent.getExtras();
        if (a != null) {

            getIntent().removeExtra("Tipo");

            String tamanio = a.getString("tam");//el numero de telefono
            String men = a.getString("Texto");//el mensaje del sms que se recibio
            String tipo = a.getString("Tipo", "fgzvs");
            Log.i("primero", men + " " + tipo);
            if (tipo.equals("M")) {
                a.clear();
                tipo = "";
                AlertDialog.Builder alert = new AlertDialog.Builder(this);

                alert.setTitle("Desea llamar al dispositivo");
                alert.setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    @SuppressLint("MissingPermission")
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + numero_disp));
                        startActivity(callIntent);
                    }
                });

                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });
                alert.show();
            }
            if(tipo.equals("llamar"))
            {
                String dial = "tel:" +numero_disp;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }
            if(tipo.equals("armar_alarma"))
            {
                lock="boton_lock";
                Log.i("main","lock"+lock);
                colocar_fondo("armar_alarma");

            }
            if(tipo.equals("desarmar_alarma"))
            {
                lock="boton_unlock";
                Log.i("main","lock"+lock);
                colocar_fondo("desarmar_alarma");
            }
            if(tipo.equals("silente"))
            {
                lock="boton_lock_silent";
                Log.i("main","lock"+lock);
                colocar_fondo("silente");
            }
            if(tipo.equals("encender"))
            {
                encendido="on";
                Log.i("main","encendido"+encendido);
                colocar_fondo("encender_motor");
                Log.i("main","entro prender el motor"+tipo);
            }
            if(tipo.equals("apagar"))
            {
                encendido="off";
                Log.i("main","encendido"+encendido);
                colocar_fondo("apagar_motor");
            }
            if (tipo.equals("getubica"))
            {
                a.clear();
                tipo="";
                //Toast.makeText(getBaseContext(), ""+men,Toast.LENGTH_LONG).show();
                String[] lines = men.split("\n");
                //   Toast.makeText(getBaseContext(), ""+lines[4].toString(),Toast.LENGTH_LONG).show();
                //  Uri uri = Uri.parse(lines[4].toString());
                // Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                this.Agregar_punto_mapa("M",men,tamanio);

            }
            if(tipo.equals("nogps"))
            {
                a.clear();
                tipo="";
                String[] lineas=men.split("\n");
                String url=lineas[4];
                String fecha=lineas[3];

                url.replace("http://maps.google.com/maps?f=q&q","");
                url.replace("&z=16","");
                String[] latlong=url.split(",");
                this.agregar_nogps(latlong,fecha,tamanio);
            }
            if(tipo.equals("montar_mapa_alerta"))
            {
                a.clear();
                tipo="";
                String[] lineas=men.split("\n");
                Toast.makeText(getBaseContext(), "Las coordenadas"+lineas,
                        Toast.LENGTH_LONG).show();


            }
            if(tipo.equals("Aviso"))
            {
                guardar_mensaje(tamanio,men);
                String aviso="";
                if(men.equals("set up fail! pls turn off ACC"))

                    aviso="ERROR INSTRUCCIÓN NO PROCESADA:\n" +
                            "El vehículo debe estar apagado. \n";
                else
                    if(men.equals("\"Set up fail! Pls close the DOOR\""))
                    aviso="ERROR INSTRUCCIÓN NO PROCESADA:\n" +
                            "Las puertas deben de estar cerradas. \n";
                Toast.makeText(getBaseContext(), aviso,
                 Toast.LENGTH_LONG).show();

            }
            if (tipo.equals("Status")) {
                guardar_mensaje(tamanio,men);
                Bundle args = new Bundle();
                args.putString("mensaje", men);
                args.putString("numero", tamanio);
                Fragment fragment = StatusFragment.newInstance("", "");
                fragment.setArguments(args);
                FragmentTransaction FT = getSupportFragmentManager().beginTransaction();
                FT.addToBackStack(null);
                FT.replace(R.id.Contenedor, fragment).commit();
            }


            if(tipo.equals("M")||tipo.equals("Z"))
            {
                guardar_mensaje(tamanio,men);
                if(tipo.equals("Z"))
                {
                    this.eliminar_puntos_zona();
                }
                this.Agregar_punto_mapa(a);
                myIntent.putExtra("Tipo","ZDONE");

            }
            if (tipo.equals("activar_area_circular"))
            {
                Log.i("main",men);
                guardar_mensaje(tamanio,men);
                actualizar_zonas_cicular();
            }
            if(tipo.equals("pintar_area_cicular"))
            {

            }

            if(tipo.equals("Alerta"))
            {
                Log.i("main",men);
                guardar_mensaje(tamanio,men);
                Bundle args = new Bundle();
                args.putString("mensaje", men);
                args.putString("numero", tamanio);
                Fragment fragment = FragmentAlert.newInstance("", "");
                fragment.setArguments(args);
                FragmentTransaction FT = getSupportFragmentManager().beginTransaction();
                FT.addToBackStack(null);
                FT.replace(R.id.Contenedor, fragment).commit();
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(this)
                                .setSmallIcon(R.drawable.ic_alarm)
                                .setContentTitle("Alarma!")
                                .setContentText(men);
                Intent resultIntent = new Intent(this, MainActivity.class);
                resultIntent.putExtra("NotiClick",true);
                resultIntent.putExtra("men",men);
                resultIntent.putExtra("numero",tamanio);
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                stackBuilder.addParentStack(MainActivity.class);

                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(
                                0,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );
                mBuilder.setContentIntent(resultPendingIntent);
                mBuilder.setAutoCancel(true);
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(12, mBuilder.build());
            }
            if(tipo.equals("Evento"))
            {
                Log.i("main",men);
                guardar_mensaje(tamanio,men);
                Bundle args = new Bundle();
                args.putString("mensaje", men);
                args.putString("numero", tamanio);
            }
            /*
            else
            {
                guardar_mensaje(tamanio,men);
            }*/
            //.setText("prueba "+tamanio);
            //Mensaje.setText(men);
        }
    }

    private void actualizar_zonas_cicular() {
        Cursor pun;
        int esta=0;
        MessagesDb zona = new MessagesDb(getBaseContext());
        zona.activar_zonas_circular();
    }

    private void agregar_nogps(String[] latlong, String fecha, String numero) {
        double lat = 0,lon = 0;
        Date fechita = null;
        try {
            lat=Double.parseDouble(latlong[0]);
            lon=Double.parseDouble(latlong[1]);
        }
        catch (Exception e){
            lat=0;lon=0;
        }

        Log.i("Fecha","recibido"+fecha);


        MessagesDb R1 = new MessagesDb(getBaseContext());
        ContentValues values = new ContentValues();
        values.put("numero",numero);
        values.put("longitud", lon);
        values.put("latitud",  lat);
        values.put("tipo","X");
        values.put("enviado",fecha.substring(2));
        R1.insertar_punto(values);
        Log.i("Punto","punto"+values.toString());
        R1.close();
        montar_mapa("","NOGPS",fecha);
    }

    private void agregar_alerta(String latlon, String fecha, String numero,String velocidad) {
        double lat = 0,lon = 0;
        Date fechita = null;
        String[] latlong=latlon.split("@");
        try {
            lat=Double.parseDouble(latlong[0]);
            lon=Double.parseDouble(latlong[1]);

        }
        catch (Exception e){
            lat=0;lon=0;

        }
        Log.i("Fecha","recibido"+fecha);
        MessagesDb R1 = new MessagesDb(getBaseContext());
        ContentValues values = new ContentValues();
        values.put("numero",numero);
        values.put("longitud", lon);
        values.put("latitud",  lat);
        values.put("tipo","!");
        values.put("enviado",fecha.substring(2));
        R1.insertar_punto(values);
        Log.i("Punto","punto"+values.toString());
        R1.close();
        montar_mapa(velocidad,"Alerta",fecha);
    }
    private boolean buscar_zona_circular(String nombre) {
        Cursor pun;
        int esta=0;
        boolean encontro = false;
        MessagesDb zona = new MessagesDb(getBaseContext());
        pun = zona.buscar_zona_circular(nombre);
        if (pun != null) {
            encontro=true;
        }
        return encontro;
    }

    public void guardar_mensaje(String numero, String mensaje) {
        //Toast.makeText(getBaseContext(), "Guardado",
        // Toast.LENGTH_LONG).show();
        MessagesDb R1 = new MessagesDb(getBaseContext());
        ContentValues values = new ContentValues();
        values.put("numero",numero);
        Log.i("Contenido",mensaje);
        values.put("Contenido",mensaje);
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        values.put("enviado",ts);

        R1.insertar_mensajes(values);
        R1.close();
        //Toast.makeText(getBaseContext(), "SMS guardado"+values,
        //      Toast.LENGTH_LONG).show();
    }

    public void eliminar_puntos_zona() {
        MessagesDb R1 = new MessagesDb(getBaseContext());
        R1.borrar_punto('Z');
    }


    public  void enviar(String mensaje) {

        PendingIntent piSent = PendingIntent.getBroadcast(getApplicationContext(),
                0, new Intent(SENT), 0);
        PendingIntent piDelivered = PendingIntent.getBroadcast(getApplicationContext(),
                0, new Intent(DELIVERED), 0);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                showProgress(false);
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "INSTRUCCIÓN ENVIADA",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "ERROR INSTRUCCIÓN NO ENVIADA: Verifique la señal GSM de su dispositivo o si el modo avión está activado",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "Sin Servicio de SMS",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter(SENT));
        //unregisterReceiver(myReceiver );

        //---when the SMS has been delivered---
        myReceiverDev = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                showProgress(false);
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "INSTRUCCION ENTREGADA",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "INSTRUCCION NO ENTREGADA",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        registerReceiver(myReceiverDev, new IntentFilter(DELIVERED));
        //unregisterReceiver(myReceiverDev );


        String phoneNo = numero_disp;
        String sms = mensaje;

        try {

            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, sms, piSent, piDelivered);

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),
                    "" + e.getMessage(),
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
    public  void enviar(String mensaje,String numero) {

        PendingIntent piSent = PendingIntent.getBroadcast(getApplicationContext(),
                0, new Intent(SENT), 0);
        PendingIntent piDelivered = PendingIntent.getBroadcast(getApplicationContext(),
                0, new Intent(DELIVERED), 0);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                showProgress(false);
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "INSTRUCCIÓN ENVIADA",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "ERROR INSTRUCCIÓN NO ENVIADA: Verifique la señal GSM de su dispositivo o si el modo avión está activado",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "Sin Servicio de SMS",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter(SENT));
        myReceiverDev = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                showProgress(false);
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "INSTRUCCION ENTREGADA",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "INSTRUCCION NO ENTREGADA",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };
        registerReceiver(myReceiverDev, new IntentFilter(DELIVERED));
        String phoneNo = numero;
        String sms = mensaje;

        try {

            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, sms, piSent, piDelivered);

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),
                    "" + e.getMessage(),
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }


    public void showProgress(final boolean show) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);

                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onStart() {
        super.onStart();
        inst = this;

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.botsydroid.controltarjeta/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }
    private void borrar() {
        try {

            Uri uriSms = Uri.parse("content://sms/inbox");
            Cursor c = this.getContentResolver().query(uriSms,
                    new String[] { "_id", "thread_id", "address",
                            "person", "date", "body" }, null, null, null);

            if (c != null && c.moveToFirst()) {
                do {
                    long id = c.getLong(0);
                    String address = c.getString(2);
                    Toast.makeText(getBaseContext(), "borrando"+id+numero_disp+address,
                            Toast.LENGTH_SHORT).show();
                    if ( address.equals(numero_disp)) {
                        this.getContentResolver().delete(
                                Uri.parse("content://sms/" + id), null, null);
                    }
                } while (c.moveToNext());
            }
        } catch (Exception e) {
        }
    }
    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.botsydroid.controltarjeta/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    public void setAlarmText(String alarmText) {
        alarmTextView.setText("Alarma de Alerta Zona!!!!");
    }

    public String getDispositivo() {
        return  desc_disp;
    }

    public String getNumero(){
        return numero_disp;
    }

    public void montar_mapa_alerta(String s,String fecha)
    {
        Intent mapita= new Intent(getBaseContext(), Mapas_alertas.class);
        String lati;
        String longi;
        String alarma_tipo;
        String[] temporal;
        String fechita=fecha;
        Toast.makeText(this, "Fecha"+fecha, Toast.LENGTH_LONG).show();
        temporal = s.split("\n");
        alarma_tipo = temporal[0];
        lati = temporal[1].replace("lat:", "");
        longi = temporal[2].replace("long:", "");
        SharedPreferences prefe = getSharedPreferences("Numero", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefe.edit();
        edit = edit.putString("latitud", lati);
        edit = edit.putString("longitud", longi);
        edit = edit.putString("alerta_tipo", alarma_tipo);
        edit=edit.putString("fecha",temporal[4]);
        edit=edit.putString("sin_gps","no");
        edit=edit.putString("fecha",fechita);
        edit.commit();
        Toast.makeText(getBaseContext(), "SharedPreferences"+edit.toString(),
                Toast.LENGTH_SHORT).show();

        startActivity(mapita);
    }
    public String boton_central()
    {
        return encendido;
    }
    public String boton_lock()
    {
        return lock;
    }

    public void montar_mapa_alerta_nogps(String cadena,String fecha) {
        Intent mapita= new Intent(getBaseContext(), Mapas_alertas.class);
        String[] lineas=cadena.split("\n");
        Toast.makeText(this, "Fecha"+fecha, Toast.LENGTH_LONG).show();
        String url=lineas[5];
        String fechita=fecha;
        String alarma_tipo;

        url=url.replace("http://maps.google.com/maps?f=q&q=","");
        url=url.replace("&z=16","");
        String[] latlong=url.split(",");
        alarma_tipo = lineas[0];
        //Toast.makeText(this, " Latitud y Longitud"+latlong[0]+"y"+latlong[1], Toast.LENGTH_LONG).show();
        SharedPreferences prefe = getSharedPreferences("Numero", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefe.edit();
        edit = edit.putString("latitud", latlong[0]);
        edit = edit.putString("longitud", latlong[1]);
        edit = edit.putString("alerta_tipo", alarma_tipo);
        edit=edit.putString("sin_gps","si");
        edit=edit.putString("fecha",fechita);
        edit=edit.putString("ultimo:",lineas[4].replace("T:",""));
        edit.commit();
        startActivity(mapita);

    }
}
