 package com.botsydroid.controltarjeta;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

 /**
 * Created by jofl on 22/10/2015.
 */
public class MessagesDb extends SQLiteOpenHelper {
    private static int version = 1;
    private static String name = "MessagesDb";
    private static SQLiteDatabase.CursorFactory factory = null;
    public MessagesDb(Context context) {
        super(context, name, factory, version);
    }
    public void onCreate(SQLiteDatabase db) {
        Log.i(this.getClass().toString(), "Creando base de datos");
        db.execSQL("CREATE TABLE valores( " +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "numero  TEXT NOT NULL, " +
                "nombre TEXT," +
                "secundario TEXT," +
                "nombre_secundario TEXT  )");
        db.execSQL("CREATE TABLE puntos" +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "numero TEXT ," +
                "latitud double NOT NULL," +
                "longitud double NOT NULL," +
                "enviado TEXT ," +
                "tipo char(1)," +
                "radio double DEFAULT 0)");
        db.execSQL("CREATE TABLE zonas (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "denominacion text not null," +
                "latitud double not null," +
                "longitud double not null," +
                "latitud2 double ,"+
                "longitud2 double ,"+
                "radio float ," +
                "activa boolean not null default 'false',"+
                "tipo text not null default 'C')");
        db.execSQL("CREATE TABLE numeros_ayudas (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "numero TEXT NOT NULL,"+
                "numero_ayuda text not null,"+
                "descripcion text not null)");
        db.execSQL("CREATE TABLE MENSAJES(" +
                "_id integer PRIMARY KEY AUTOINCREMENT," +
                "enviado DATETIME DEFAULT CURRENT_TIMESTAMP," +
                "numero TEXT NOT NULL," +
                "tipo text not null default 'N',"+
                "contenido TEXT NOT NULL)");
        db.execSQL("CREATE TABLE ESTADO(" +
                "_id integer PRIMARY KEY AUTOINCREMENT," +
                "numero TEXT not null," +
                "recibido DATETIME DEFAULT CURRENT_TIMESTAMP not null," +
                "boton_lock TEXT not null," +
                "boton_central TEXT not null," +
                "boton_panico TEXT not null)");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void insertar(ContentValues values )
    {
        SQLiteDatabase database = this.getWritableDatabase();
        //ContentValues values = new ContentValues();
        //values.put("StudentName", queryValues.get("StudentName"));
        database.insert("valores", null, values);
        database.close();
        Log.d(this.getClass().toString(), "Insertado");

    }
    public void insertar_numero_ayuda(ContentValues values)
    {
        SQLiteDatabase database=this.getWritableDatabase();
        database.insert("numeros_ayudas",null,values);
        database.close();
        Log.d(this.getClass().toString(),"numero de ayuda agregado");
    }
    public void insertar_punto(ContentValues values)
    {
        SQLiteDatabase database = this.getWritableDatabase();
        database.insert("puntos", null, values);
        database.close();
        Log.d(this.getClass().toString(), "Insertado");

    }
    public void insertar_estado(ContentValues values)
    {
        SQLiteDatabase database=this.getWritableDatabase();
        database.insert("estado",null,values);
        database.close();
    }
    public void insert_zone(ContentValues values)
    {
        SQLiteDatabase database=this.getWritableDatabase();
        database.insert("zonas",null,values);
        database.close();
        Log.d(this.getClass().toString(),"Insertado");
    }

    public void modificar_punto(double radio_zona, int id)
    {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("update puntos set radio="+radio_zona+" where _id="+id);
        Log.d(this.getClass().toString(), "Modificado");

    }
    public void borrar_punto(char tipo)
    {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("delete from puntos where tipo='"+tipo+"'");
        Log.d(this.getClass().toString(), "Eliminado");
    }

    public void borrar_mensaje(int cual)
    {
        SQLiteDatabase database=this.getWritableDatabase();
        database.execSQL("delete from MENSAJES where _id="+cual);
        Log.d(this.getClass().toString(),"mensaje borrado");
    }
    public void insertar_mensajes(ContentValues values)
    {
        SQLiteDatabase database=this.getWritableDatabase();
        database.insert("MENSAJES",null,values);
        database.close();
        Log.d(this.getClass().toString(),"Insertado");
    }
    /*
    public int cantidad()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCount= db.rawQuery("select count(*) from valores", null);
        mCount.moveToFirst();
        int count= mCount.getInt(0);
        mCount.close();
        return count;
    }
    public int cantidad(String tipo)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCount= db.rawQuery("select count(*) from valores where nombre="+tipo, null);
        mCount.moveToFirst();
        int count= mCount.getInt(0);
        mCount.close();
        return count;
    }
    public float promedio(String tipo){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCount= db.rawQuery("select AVG(valor) from valores where nombre="+tipo , null);
        mCount.moveToFirst();
        float count= mCount.getFloat(0);
        mCount.close();
        return count;
    }
    public float promedio()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCount= db.rawQuery("select AVG(valor) from valores", null);
        mCount.moveToFirst();
        float count= mCount.getFloat(0);
        mCount.close();
        return count;
    }*/
    public Cursor datos(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCount= db.rawQuery("select  _id, numero, nombre from valores order by nombre DESC limit 20", null);
        return mCount;
    }
    public Cursor datos(String tipo){
        SQLiteDatabase db = this.getWritableDatabase();
        String[] campos = new String[] {"valor"};
        String []args = new String[] {tipo};
        Cursor mCount = db.query("valores",campos,"nombre =?",args,null,null,"_id DESC","20");
        return mCount;
    }
    public Cursor datos_puntos(char filtro,int cantidad){
        SQLiteDatabase db = this.getWritableDatabase();
        String sql="select  _id, numero,latitud, longitud ,enviado from puntos where tipo='"+filtro+"'order by enviado DESC limit "+cantidad;
        Log.i("sql","Sql"+sql);
        Cursor mCount= db.rawQuery(sql, null);
        return mCount;
    }
    public Cursor datos_estado(int cantidad)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        String sql="select _id,boton_central,boton_lock,boton_sos from estado order by enviado DESC limit "+cantidad;
        Cursor mCount=db.rawQuery(sql,null);
        return mCount;
    }
    public Cursor ultimo_punto(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCount= db.rawQuery("select  _id, latitud, longitud ,enviado from puntos where tipo!='Z' order by enviado DESC limit 1", null);
        return mCount;
    }
    public Cursor punto_zona(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCount= db.rawQuery("select  _id, latitud, longitud ,enviado,radio from puntos where tipo='Z' order by enviado DESC limit 1", null);
        return mCount;
    }
    public Cursor zona_mapa(String tipo){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCount=db.rawQuery("select denominacion,latitud,longitud,latitud2,longitud2,radio,activa from zonas where tipo=\""+tipo+"\"",null);
        return mCount;
    }
    public Cursor buscar_zona_circular(String nombre){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor mCount=db.rawQuery("select denominacion,radio,activa from zonas where denominacion=\""+nombre+"\"",null);
        if(mCount!=null){
            return mCount;
        }
        else
            return null;

    }
    public Cursor activar_zonas_circular()
    {
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor mCount=db.rawQuery("update zonas set activa=\'true\'",null);
        if(mCount!=null)
        {
            return mCount;
        }
        else
            return null;
    }
    public Cursor buscar_zona_rectangular(String tipo)
    {
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor mCount=db.rawQuery("select latitud,longitud,latitud2,longitud from zonas where tipo=\"R\"",null);
        if(mCount!=null)
            return mCount;
        else return null;
    }
    public Cursor datosmensaje(String numero){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCount= db.rawQuery("select  _id, numero, contenido, enviado from MENSAJES where numero='\"+numero+\"' order by enviado DESC ", null);
        return mCount;
    }
    public Cursor datosAlarma(String numero){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCount= db.rawQuery("select  _id, numero, contenido, enviado from MENSAJES where contenido like '%alarm!%' and numero='"+numero+"'  order by enviado desc,numero", null);
        return mCount;
    }
    public Cursor numero_registrado(String numero)
    {
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor mCount=db.rawQuery("select _id,numero,nombre from valores where numero="+numero,null);
        return mCount;
    }
    public Cursor datos_mensaje(int id){
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor mCount=db.rawQuery("select _id,numero,contenido from MENSAJES  where _id= "+id,null);
        return mCount;
    }
     public Cursor buscar_numeros_emergengia(String filtro){
         SQLiteDatabase db = this.getReadableDatabase();
         String sql="select  numero_ayuda from numeros_ayudas where numero='"+filtro+"'";
         Log.i("sql","Sql"+sql);
         Cursor mCount= db.rawQuery(sql, null);
         return mCount;
     }
     public Cursor buscar_estado(String filtro){
         SQLiteDatabase db = this.getReadableDatabase();
         String sql="select  boton_central,boton_lock,boton_panico from estado where numero='"+filtro+"'order by enviado desc limit 1";
         Log.i("sql","Sql"+sql);
         Cursor mCount= db.rawQuery(sql, null);
         return mCount;
     }

 }
