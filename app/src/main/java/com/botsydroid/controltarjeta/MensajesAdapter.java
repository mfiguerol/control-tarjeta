package com.botsydroid.controltarjeta;

import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

public class MensajesAdapter extends CursorAdapter {
    public MensajesAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }



    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.elemento_lista, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView tvFecha = (TextView) view.findViewById(R.id.tvFecha);
        ImageView Alerta=(ImageView)view.findViewById(R.id.ivAlerta);
        TextView tvTipo= (TextView) view.findViewById(R.id.tvTipo);
        TextView tvFecha_gps=(TextView) view.findViewById(R.id.tv_fecha_gps);
        TextView tv_direccion_alerta=(TextView) view.findViewById(R.id.tv_direccion_alerta);
        tv_direccion_alerta.setVisibility(View.INVISIBLE);
        ConstraintLayout contenedor_item=(ConstraintLayout) view.findViewById(R.id.contenedor_item);
        tvFecha_gps.setText("");
       // TextView tvIndice=(TextView) view.findViewById(R.id.tvIndice);
        // Extract properties from cursor



        String numero = cursor.getString(cursor.getColumnIndexOrThrow("_id"));
        String contenido = cursor.getString(cursor.getColumnIndexOrThrow("contenido"));
        String[] varios =contenido.split("\n");
        String alarma_tipo=varios[0].trim();


        if(!varios[3].contains("Last:")) {
            String[] fecha = varios[4].split(" ");
            String[] fecha_final=fecha[0].split("/");
            fecha_final[0]=fecha_final[0].replace("T:","20");
            tvFecha.setText("Hora "+fecha[1]+", "+fecha_final[2]+"/"+fecha_final[1]+"/"+fecha_final[0]);


//            tvIndice.setText(numero);
        }
        else{
            String[] fecha = varios[2].split(" ");
            String[] fecha_final=fecha[0].split("/");
            fecha_final[0]=fecha_final[0].replace("T:","20");
            try {
                tvFecha.setText("Hora "+fecha[1]+", "+fecha_final[2]+"/"+fecha_final[1]+"/"+fecha_final[0]);
            }
            catch (Exception e)
            {
                String ultima=varios[4].replace("T:","");
                tvFecha.setText("Error desde Hora: "+ultima);
            }

            String ultima=varios[4].replace("T:","");
            tvFecha_gps.setText("Sin gps desde="+ultima);

//
        }

        if(alarma_tipo.equalsIgnoreCase("Power Alarm!")) {
            tvTipo.setText("ALERTA DE ENERGIA");
            Alerta.setImageResource(R.drawable.alerta_power_bajo);
        }
        if(alarma_tipo.equalsIgnoreCase("Door alarm!"))
        {
            tvTipo.setText("ALERTA DE PUERTA");
            Alerta.setImageResource(R.drawable.alerta_puerta_bajo);
        }
        if(alarma_tipo.equalsIgnoreCase("Battery Alarm!")) {
            tvTipo.setText("ALERTA DE BATERIA");
            Alerta.setImageResource(R.drawable.alerta_bateria_bajo);

        }
        if(alarma_tipo.equalsIgnoreCase("Sensor Alarm!"))
        {
            tvTipo.setText("ALERTA DEL SENSOR ANTIROBO");
            Alerta.setImageResource(R.drawable.alerta_robo_bajo);

        }
        if(alarma_tipo.equalsIgnoreCase("Help Me!"))
        {
            tvTipo.setText("BOTON DE PANICO");
            Alerta.setImageResource(R.drawable.alerta_panic_bajo);

        }
        if(alarma_tipo.equalsIgnoreCase("Move!"))
        {
            tvTipo.setText("ALERTA DE MOVIMIENTO");
            Alerta.setImageResource(R.drawable.alerta_movilidad_bajo);

        }
        if(alarma_tipo.equalsIgnoreCase("speed!"))
        {
            tvTipo.setText("ALERTA DE VELOCIDAD");
            Alerta.setImageResource(R.drawable.alerta_velocidad_bajo);

        }
        if(alarma_tipo.contains("ACC"))
        {
            tvTipo.setText("ALERTA DE IGNICION");
            Alerta.setImageResource(R.drawable.alerta_ignicion_bajo);

        }
        if(alarma_tipo.toLowerCase().contains("oil"))
        {
            tvTipo.setText("ALERTA DE COMBUSTIBLE");
            Alerta.setImageResource(R.drawable.alerta_combustible_bajo);


        }


    }



}
