package com.botsydroid.controltarjeta;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentAlert.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentAlert#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentAlert extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String url="nada",latlong[];

    private String alarma_tipo="nada";
    private OnFragmentInteractionListener mListener;
    private LatLng ubicacion_dispositivo;

    public FragmentAlert() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentAlert.
     */
    Activity activity ;
    // TODO: Rename and change types and number of parameters
    public static FragmentAlert newInstance(String param1, String param2) {
        FragmentAlert fragment = new FragmentAlert();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_alert, container, false);
        //  LinearLayout auxlayout = (LinearLayout) view.findViewById(R.id.alarma_contenido);

        ImageButton btmapa=(ImageButton)view.findViewById(R.id.stopAlarm);
        ImageView tipo_alarm= (ImageView)view.findViewById(R.id.tipo_alarma);
        TextView titulo_alarma=(TextView)view.findViewById(R.id.titulo_alarma);
        TextView sin_gps=(TextView) view.findViewById(R.id.nogps);



        Toast.makeText(getContext(), url,
                Toast.LENGTH_LONG).show();
        final String mensaje = getArguments().getString("mensaje");
        final String numero=getArguments().getString("numero");
        final String[] valores;
        String mensaje_ayuda=null;
        activity= getActivity();


        final String[] varios =mensaje.split("\n");

        alarma_tipo=varios[0].trim();

      /*  Toast.makeText(getContext(), "Mensaje:"+ varios[0]+ varios[1]+ varios[2]+varios[3]+varios[4]+varios[5],
                Toast.LENGTH_LONG).show();*/
        Log.i("Mensaje Alerta Mapa","Mensaje:"+ varios[0]+ varios[1]+ varios[2]+varios[3]+varios[4]+varios[5]);
        if(mensaje.contains("Last:")) {
            sin_gps.setVisibility(View.VISIBLE);
        }

        //latlong[1]=varios[2];
        url=varios[4];
        btmapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fecha=varios[4];
             /*   Toast.makeText(getContext(), "Coordenadas:"+ latlong[0]+":"+latlong[1],
                        Toast.LENGTH_LONG).show();*/

                if(activity instanceof MainActivity) {
                    MainActivity myactivity = (MainActivity) activity;
                    //Toast.makeText(getContext(), "SharedPreferences"+mensaje,Toast.LENGTH_LONG).show();
                    if(!mensaje.contains("Last:"))
                    {
                        fecha=varios[4];
                        myactivity.montar_mapa_alerta(mensaje, fecha);
                    }
                    else
                    {
                        fecha=varios[2];
                        myactivity.montar_mapa_alerta_nogps(mensaje, fecha);
                    }


                }
            }
        });


        Log.i("tipo alarma",alarma_tipo);

        if(alarma_tipo.equalsIgnoreCase("acc alarm!"))
        {
            tipo_alarm.setImageResource(R.drawable.alerta_ignicion_bajo);
            mensaje_ayuda=getString(R.string.ACConoff);
            titulo_alarma.setText("ALERTA DE IGNICION");
        }
        if(alarma_tipo.equalsIgnoreCase("acc on!"))
        {
            tipo_alarm.setImageResource(R.drawable.alerta_ignicion_bajo);
            mensaje_ayuda=getString(R.string.ACCon);
            titulo_alarma.setText("ALERTA DE ENCENDIDO");
        }
        if(alarma_tipo.equalsIgnoreCase("acc off!"))
        {
            tipo_alarm.setImageResource(R.drawable.alerta_ignicion_bajo);
            mensaje_ayuda=getString(R.string.ACCoff);
            titulo_alarma.setText("ALERTA DE APAGADO");
        }
        if(alarma_tipo.equalsIgnoreCase("Power Alarm!")) {
            titulo_alarma.setText("ALARMA DE ENERGIA");
            tipo_alarm.setImageResource(R.drawable.alerta_power_bajo);
            mensaje_ayuda = getString(R.string.poweralarm);
        }
        if(alarma_tipo.equalsIgnoreCase("Door alarm!"))
        {
            tipo_alarm.setImageResource(R.drawable.alerta_puerta_bajo);
            mensaje_ayuda = getString(R.string.door);
            titulo_alarma.setText("ALARMA DE PUERTA");

        }
        if(alarma_tipo.equalsIgnoreCase("Battery Alarm!")) {
            tipo_alarm.setImageResource(R.drawable.alerta_bateria_bajo);
            mensaje_ayuda = getString(R.string.batteryalarm);
            titulo_alarma.setText("ALERTA DE BATERIA");
        }
        if(alarma_tipo.equalsIgnoreCase("sensor Alarm!"))
        {
            tipo_alarm.setImageResource(R.drawable.alerta_robo_bajo);
            mensaje_ayuda =getString(R.string.sensoralarm);
            titulo_alarma.setText("ALERTA DE SENSOR ANTIROBO");
        }
        if(alarma_tipo.equalsIgnoreCase("Help Me!"))
        {
            tipo_alarm.setImageResource(R.drawable.alerta_panic_bajo);
            mensaje_ayuda=getString(R.string.helpme);
            titulo_alarma.setText("ALERTA DE BOTON DE PANICO");
        }
        if(alarma_tipo.equalsIgnoreCase("Move!"))
        {
            tipo_alarm.setImageResource(R.drawable.alerta_movilidad_bajo);
            mensaje_ayuda=getString(R.string.move);
            titulo_alarma.setText("ALERTA DE MOVIMIENTO");
        }
        if(alarma_tipo.equalsIgnoreCase("speed!"))
        {
            tipo_alarm.setImageResource(R.drawable.alerta_velocidad_bajo);
            mensaje_ayuda=getString(R.string.speed);
            titulo_alarma.setText("ALERTA DE VELOCIDAD");
        }
        if(alarma_tipo.toLowerCase().contains("oil"))
        {
            tipo_alarm.setImageResource(R.drawable.alerta_combustible_bajo);
            mensaje_ayuda=getString(R.string.oil);
            titulo_alarma.setText("ALERTA DE COMBUSTIBLE");

        }
        if(alarma_tipo.equalsIgnoreCase("No gps!")) {
            titulo_alarma.setText("GPS INHABILITADO");
            tipo_alarm.setImageResource(R.drawable.gps_inactivo_bajo);
            mensaje_ayuda = getString(R.string.nogps);
        }
        if(alarma_tipo.equalsIgnoreCase("LOW BATTERY!")) {
            titulo_alarma.setText("ALERTA DE BATERIA BAJA");

            tipo_alarm.setImageResource(R.drawable.alerta_bateria_bajo);
            mensaje_ayuda = getString(R.string.batterylow);
        }
        if(alarma_tipo.equalsIgnoreCase("stockade!")) {
            titulo_alarma.setText("ALERTA DE GEOZONA RECTANGULAR");
            Double lat, lon;
            String lati = varios[1].replace("lat:", "");
            String longi = varios[2].replace("long:", "");
            lat = Double.valueOf(lati);
            lon = Double.valueOf(longi);
            if (buscar_zona_rectangular(lat, lon) == 1) {
                titulo_alarma.setText("ALERTA DE ENTRADA A GEOZONA RECTANGULAR");
                /*if (((MainActivity) activity).GeoFence_activo == true)
                    myactivity.enviar("Stop" + myactivity.getClave());
            } else {
                titulo_alarma.setText("ALERTA DE SALIDA A GEOZONA RECTANGULAR");
                if (((MainActivity) activity).GeoFence_activo == true)
                    myactivity.enviar("Stop" + myactivity.getClave());*/

            }
            tipo_alarm.setImageResource(R.drawable.alerta_geozonacuadrada_bajo);
            mensaje_ayuda=getString(R.string.stockade);
        }


        if(alarma_tipo.equalsIgnoreCase("PowerAlarm!"))
        {
            tipo_alarm.setImageResource(R.drawable.alerta_power_bajo);
            mensaje_ayuda=getString(R.string.poweralarm);


        }
        url=varios[1]+"\n"+varios[2]+"\n"+varios[3]+"\n"+varios[4];
        TextView auxiliar=(TextView)view.findViewById(R.id.mensaje_alarma);
        auxiliar.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        auxiliar.setText(mensaje_ayuda);

        return view;

    }
    private int buscar_zona_rectangular(Double lat,Double lon) {
        Cursor pun;
        int esta=0;
        MessagesDb zona = new MessagesDb(getContext());
        pun = zona.buscar_zona_rectangular("R");
        if (pun != null) {
            pun.moveToFirst();
            int donde;
            donde = pun.getColumnIndex("latitud");
            try {
                double latitud = pun.getDouble(donde);
                donde = pun.getColumnIndex("longitud");
                double longitud = pun.getDouble(donde);
                donde = pun.getColumnIndex("latitud2");
                double latitud2 = pun.getDouble(donde);
                donde = pun.getColumnIndex("longitud2");
                double longitud2 = pun.getDouble(donde);
                LatLng punto=new LatLng(lat,lon);
                LatLng x1=new LatLng(latitud,longitud);
                LatLng x2=new LatLng(latitud2,longitud2);
                LatLng x3=new LatLng(latitud,longitud);
                LatLng x4=new LatLng(latitud2,longitud2);
                List<LatLng> poligono=new ArrayList<LatLng>();
                poligono.clear();
                poligono.add(x1);
                poligono.add(x2);
                poligono.add(x3);
                poligono.add(x4);

                if(PolyUtil.containsLocation(punto,poligono,false))
                    esta=1;
                else
                    esta=-1;
            } catch (Exception e) {
                return 0;
            }
        }
        return esta;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
