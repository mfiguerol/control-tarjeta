package com.botsydroid.controltarjeta;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;


import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Mapas extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener{

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation, ultimo = null,ultima_zona=null;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    private FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;
    boolean zona_circular = false;
    boolean zona_rectangular=false;
    double tam_zona;
    private double milatitud;
    private double milongitud;
    private LatLng ubicacion_dispositivo;
    private float CuantoZoom=13;
    private Vibrator vibrator;
    private  List<LatLng> poligono=new ArrayList<LatLng>();
    private static String SENT = "SMS_SENT";
    private static String DELIVERED = "SMS_DELIVERED";
    private static int MAX_SMS_MESSAGE_LENGTH = 160;
    private String Speed;
    String numero_disp;
    String clave;
    String tipos_zonas[] = {"Seleccione","Zona Rectangular"};
    private Spinner spinner;
    private String unidad_medida;
    LatLngBounds.Builder bordes = new LatLngBounds.Builder();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        buildGoogleApiClient();
        String[]valor;
        setContentView(R.layout.activity_mapas);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        Context contexto =  this.getApplicationContext();
        vibrator=(Vibrator)contexto.getSystemService(VIBRATOR_SERVICE);
        mapFragment.getMapAsync(this);
        mapFragment.getView().setClickable(false);



    }

    public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = null;
        mMap = googleMap;
        SharedPreferences prefe = getSharedPreferences("Numero", Context.MODE_PRIVATE);
        final String fecha = prefe.getString("fecha", "0.0");
        final String completo = prefe.getString("varios", "");
        Speed=prefe.getString("velocidad","0.0");
        CuantoZoom= mMap.getCameraPosition().zoom;
        TextView gps=(TextView) findViewById(R.id.tv_gps);
        if(!completo.equals("NOGPS")) {

            String[] valor;
            gps.setVisibility(View.INVISIBLE);
            valor = completo.split(" ");
            String[] valores = valor[0].split(":");
            ImageView auxiliar1=(ImageView)findViewById(R.id.ignicion);
            auxiliar1.setVisibility(View.VISIBLE);
            if (valor[1].equalsIgnoreCase("ON")) {
                ImageView auxiliar = (ImageView) findViewById(R.id.power);
                auxiliar.setImageResource(R.drawable.energia_activo);
                auxiliar.setVisibility(View.VISIBLE);
            } else {
                ImageView auxiliar = (ImageView) findViewById(R.id.power);
                auxiliar.setImageResource(R.drawable.energia_desactivado);
                auxiliar.setVisibility(View.VISIBLE);
            }
            if (valor[3].equalsIgnoreCase("OFF")) {
                ImageView auxiliar = (ImageView) findViewById(R.id.puerta);
                auxiliar.setImageResource(R.drawable.puerta_cerrada);
            } else {
                ImageView auxiliar = (ImageView) findViewById(R.id.puerta);
                auxiliar.setImageResource(R.drawable.puerta_abierta);
            }
            if (valor[5].equalsIgnoreCase("ON")) {
                ImageView auxiliar = (ImageView) findViewById(R.id.ignicion);
                auxiliar.setVisibility(View.VISIBLE);
                auxiliar.setImageResource(R.drawable.ignicion_inactivo);
            } else {
                ImageView auxiliar = (ImageView) findViewById(R.id.ignicion);
                auxiliar.setImageResource(R.drawable.ignicion_activo);
                auxiliar.setVisibility(View.VISIBLE);
            }
        }//cuando llega todo normal del carro (hay gps)
        else
        {
            if(completo.equals("NOGPS"))
            {

                gps.setText("Sin GPS");
                gps.setVisibility(View.VISIBLE);
                ImageView power=(ImageView)findViewById(R.id.power);
                power.setVisibility(View.INVISIBLE);
                ImageView puerta=(ImageView)findViewById(R.id.puerta);
                puerta.setVisibility(View.INVISIBLE);
                ImageView ignicion=(ImageView)findViewById(R.id.ignicion);
                ignicion.setVisibility(View.INVISIBLE);
                ImageView speed=(ImageView)findViewById(R.id.velocidad);
                speed.setVisibility(View.INVISIBLE);
                ImageButton distancia=(ImageButton) findViewById(R.id.distancia);
                distancia.setVisibility(View.INVISIBLE);

            }

        }
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                Log.d("Map", "Map clicked");
                if ((zona_circular==true)||(zona_rectangular==true))
                    drawMarker(point);
            }


        });
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {
                if(!marker.getTitle().equals("Dispositivo de monitoreo"))
                {
                    String todo;
                    if(!completo.equals("NOGPS")) {
                        todo = "Enviado: " + fecha + "\n Lat.:" + marker.getPosition().latitude + "\n Long.:" + marker.getPosition().longitude;
                    }
                    else {
                        todo = "Ultima ubicación conocida" + fecha + "\n Lat.:" + marker.getPosition().latitude + "\n Long.:" + marker.getPosition().longitude;
                    }
                    show_alert(todo);
                }
                return false;
            }
        });
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            public void onMapLoaded() {
                //do stuff here
                set_distancia();
            }
        });

        latLng = cargar_puntos_mapa();


        if (latLng != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, CuantoZoom);
            mMap.animateCamera(cameraUpdate);
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

        } else {
            //Button auxiliar = (Button) (findViewById(R.id.config_bt_zona));
            //auxiliar.setEnabled(false);
            // Button auxilia2 = (Button) (findViewById(R.id.distancia));
            //auxilia2.setEnabled(false);

            latLng = new LatLng(0, 0);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, CuantoZoom);
            mMap.animateCamera(cameraUpdate);
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
        }

    }

    private void show_alert(String todo) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("INFORMACION");
        alert.setMessage(todo);
        alert.setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        alert.show();
    }

    public void ver_mas(View view)
    {
        set_distancia();
    }
    public void activar_geocerca(View view)
    {    set_zona();
    }

    private void set_zona() {
       // ImageButton aux=(ImageButton) findViewById(R.id.alarma_imagen);
     //   aux.setVisibility(View.INVISIBLE);
        Toast.makeText(getApplicationContext(), "Pulse 2 veces en el mapa para generar una Geocerca rectangular",
                Toast.LENGTH_LONG).show();
        zona_rectangular=true;
    }

    public void set_distancia()
    {
        TextView aux_velocidad=(TextView) findViewById(R.id.tv_velocidad);
        TextView aux=(TextView)findViewById(R.id.tv_distancia);
        Speed.replace("speed","Velocidad");
        aux.setText(calcular_distancia());
        aux_velocidad.setText(Speed);
    }

    private LatLng cargar_puntos_mapa() {
        Cursor pun;
        LatLng dibujar=null;
        double latitud = 0, longitud = 0;
        int donde;
        MessagesDb todos = new MessagesDb(getBaseContext());
        String fecha = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        pun = todos.datos_puntos('M',1);
        if (pun != null && pun.getCount() > 0) {

            ImageButton auxilia2 = (ImageButton) (findViewById(R.id.distancia));
            auxilia2.setBackgroundResource(R.drawable.distancia_activo);
            auxilia2.setEnabled(true);

            pun.moveToFirst();
            do {
                donde=pun.getColumnIndex("latitud");
                latitud = pun.getDouble(donde);
                donde=pun.getColumnIndex("longitud");
                longitud = pun.getDouble(donde);
                dibujar = new LatLng(latitud, longitud);
                String dispositivo;
                donde=pun.getColumnIndex("numero");
                dispositivo=pun.getString(donde);


                Bitmap bmp = BitmapFactory.decodeResource(
                        getBaseContext().getResources(), R.drawable.localizacion_mallku);
                Bitmap scaled = Bitmap.createScaledBitmap(bmp, 60, 80, false);
                mMap.addMarker(new MarkerOptions().position(dibujar).title(dispositivo)
                        .icon(BitmapDescriptorFactory.fromBitmap(scaled))
                );


            } while (pun.moveToNext());
        }
        else
        {
            dibujar=null;
        }
        //Para los puntos dibujados en el mapa directamente primero los Circulos
        pun = todos.zona_mapa("C");
        if (pun != null && (pun.getCount() <= 5 &&pun.getCount()>0)) {
            pun.moveToFirst();
            do{
                donde=pun.getColumnIndex("denominacion");
                String nombre=pun.getString(donde);
                donde=pun.getColumnIndex("latitud");
                latitud=pun.getDouble(donde);
                donde=pun.getColumnIndex("longitud");
                longitud=pun.getDouble(donde);
                donde=pun.getColumnIndex("radio");
                float radio=pun.getFloat(donde);
                dibujar = new LatLng(latitud, longitud);
                dibujar_zona(radio,dibujar,nombre,false);
            }while(pun.moveToNext());
        }
        else {
            dibujar = null;
        }
        pun.close();
        pun = todos.zona_mapa("R");
        if (pun != null && pun.getCount() ==1 ) {
            Toast.makeText(getApplicationContext(), "Tiene una zona rectangular",
                    Toast.LENGTH_LONG).show();
            pun.moveToFirst();
            do{
                double lat2,lon2;
                donde=pun.getColumnIndex("denominacion");
                String nombre=pun.getString(donde);
                donde=pun.getColumnIndex("latitud");
                latitud=pun.getDouble(donde);
                donde=pun.getColumnIndex("longitud");
                longitud=pun.getDouble(donde);
                donde=pun.getColumnIndex("latitud2");
                lat2=pun.getDouble(donde);
                donde=pun.getColumnIndex("longitud2");
                lon2=pun.getDouble(donde);
                dibujar = new LatLng(latitud, longitud);
                LatLng dibujar2;
                dibujar2=new LatLng(lat2,lon2);
                poligono.clear();
                poligono.add(dibujar);
                poligono.add(dibujar2);
                simular_cuadrado(nombre,false);
            }while(pun.moveToNext());
        }
        else {
            dibujar = null;
        }

        return dibujar;
    }



    private void drawMarker(final LatLng point) {
        final float[] zona_circulo = {0};
       /* if(zona_circular==true) {
            pedir_datos_zona_circular(point);
        }*/
        if(zona_rectangular==true){
            poligono.add(point);
            int cuantos=poligono.size();
            if(cuantos>=2)
            {
                pedir_datos_zona_poligonar();

            }

        }
    }
    private void pedir_datos_zona_poligonar() {
        LayoutInflater linf = LayoutInflater.from(this);
        final View inflator = linf.inflate(R.layout.agregar_zona_rectangular, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Definición de Zona");
        alert.setView(inflator);
        final EditText et1 = (EditText) inflator.findViewById(R.id.nombre);
        alert.setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String nombre = et1.getText().toString();
                int cuantos=poligono.size();
                simular_cuadrado(nombre,true);

            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });

        alert.show();
    }

    private void simular_cuadrado(String nombre,boolean guardar) {
        LatLng tercer,cuarto,temporal1,temporal2;
        temporal1=poligono.get(0);
        temporal2=poligono.get(1);
        LatLng point=temporal1;
        // mMap.addMarker(new MarkerOptions().position(point).title("Marcador de Zona Rectangular: "+nombre));
        point=temporal2;
        // mMap.addMarker(new MarkerOptions().position(point).title("Marcador de Zona Rectangular: "+nombre));
        tercer=new LatLng(temporal1.latitude,temporal2.longitude);
        cuarto=new LatLng(temporal2.latitude,temporal1.longitude);
        poligono.clear();
        poligono.add(temporal1);
        poligono.add(tercer);//para el tercer punto
        poligono.add(temporal2);
        poligono.add(cuarto);//para el cuarto punto
        int cuantos=poligono.size();
        dibujar_zona_poligonar(cuantos);
        if(guardar) {

            Agregar_zona_poligonar(temporal1, temporal2, nombre);
        }
    }

    private void Agregar_zona_poligonar(LatLng temporal1,LatLng temporal2,String nombre) {
        MessagesDb R1 = new MessagesDb(getBaseContext());
        ContentValues values = new ContentValues();
        values.put("denominacion",nombre);
        values.put("latitud",temporal1.latitude);
        values.put("longitud",temporal1.longitude);
        values.put("latitud2",temporal2.latitude);
        values.put("longitud2",temporal2.longitude);
        values.put("tipo","R");
        values.put("activa",true);
        R1.insert_zone(values);
        R1.close();
        poligono.clear();
        double mayorX,menorX,mayorY,menorY;
        if(Math.abs(temporal1.longitude)<Math.abs(temporal2.longitude))
        {
            mayorX = temporal2.longitude;
            menorX = temporal1.longitude;
        }
        else {
            mayorX = temporal1.longitude;
            menorX = temporal2.longitude;
        }
        if(Math.abs(temporal1.latitude)<Math.abs(temporal2.latitude))
        {
            mayorY=temporal2.latitude;
            menorY=temporal1.latitude;
        }
        else {
            mayorY=temporal1.latitude;
            menorY=temporal2.latitude;
        }
        SharedPreferences prefe = getSharedPreferences("Numero", Context.MODE_PRIVATE);
        clave = prefe.getString("clave", "0000");
        enviar("stockade"+clave+" "+mayorY+","+menorX+";"+menorY+","+mayorX);
    }


    private void pedir_datos_zona_circular(final LatLng point)
    {
        final float[] zona_circulo = {0};
        LayoutInflater linf = LayoutInflater.from(this);
        final View inflator = linf.inflate(R.layout.agregar_zona, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Definición de Zona");
        alert.setView(inflator);
        final EditText et1 = (EditText) inflator.findViewById(R.id.tam_zona);
        final EditText et2 = (EditText) inflator.findViewById(R.id.nombre);

        alert.setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String s1 = et1.getText().toString();
                String s2 = et2.getText().toString();
                zona_circulo[0] =Float.parseFloat(s1);

                if (zona_circulo[0]!=0&&zona_circulo[0]<=999) {
                    //dibujar_zona(zona_circulo[0],point,s2,true);
                    if(!s2.contains(" ")) {
                        SharedPreferences prefe = getSharedPreferences("Numero", Context.MODE_PRIVATE);
                        clave = prefe.getString("clave", "0000");
                        MessagesDb R1 = new MessagesDb(getBaseContext());
                        ContentValues values = new ContentValues();
                        values.put("denominacion",s2);
                        values.put("latitud",point.latitude);
                        values.put("longitud",point.longitude);
                        values.put("tipo","C");
                        values.put("activa",false);
                        R1.insert_zone(values);
                        R1.close();
                        String tama=String.format("%3s", (int)zona_circulo[0]).replace(' ', '0');
                        enviar("area" + clave + " " + point.latitude + "," + point.longitude + " "+s2 +","+tama + unidad_medida.toUpperCase());

                    }
                    else
                    {


                        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                                .findFragmentById(R.id.map);
                        Snackbar snackbar = Snackbar
                                .make(getWindow().getDecorView().getRootView(), "No debe tener espacios en blanco el nombre del área", Snackbar.LENGTH_LONG);

                        snackbar.show();

                    }

                }

            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });

        alert.show();
    }
    private void dibujar_zona_poligonar(int cuantos) {
        PolygonOptions poly = new PolygonOptions();
        poly.fillColor(Color.argb(67, 51, 20, 129));
        poly.strokeColor(Color.argb(116,51,20,129));
        poly.visible(true);
        for(int i = 0; i < cuantos; i++)
        {
            poly.add(poligono.get(i));
        }

        mMap.addPolygon(poly);

    }
    private void dibujar_zona(float tama,LatLng temporal,String nombre,boolean agregar) {
        CircleOptions circleOptions = new CircleOptions()
                .center(temporal)
                .radius(tama)
                .strokeColor(Color.argb(116, 129, 20, 51))
                .fillColor(Color.argb(67, 129, 20, 51))
                .visible(true);
        Circle circle = mMap.addCircle(circleOptions);
        mMap.addMarker(new MarkerOptions().position(temporal).title("Marcador de Zona Circular: "+nombre));
        if (agregar){
            Agregar_zona(temporal,tama,nombre);
            zona_circular=false;
            zona_rectangular=false;


        }


    }

    private void Agregar_zona(LatLng temporal, float tama,String nombre) {
        MessagesDb R1 = new MessagesDb(getBaseContext());
        ContentValues values = new ContentValues();
        values.put("denominacion",nombre);
        values.put("latitud",temporal.latitude);
        values.put("longitud",temporal.longitude);
        values.put("tipo", "C");
        values.put("activa",true);
        values.put("radio",tama);
        R1.insert_zone(values);
        Toast.makeText(getApplicationContext(), "Zona guardada"+values,
                Toast.LENGTH_LONG).show();
        R1.close();
        SharedPreferences prefe = getSharedPreferences("Numero", Context.MODE_PRIVATE);
        clave = prefe.getString("clave", "0000");
        String tamanio=String.valueOf(tama);
        tamanio=String.format("%3s", tamanio).replace(' ', '0');
        enviar("area"+clave+" "+temporal.latitude+","+temporal.longitude+" "+nombre+","+tamanio+"M");
        // enviar("area"+clave+" "+"on");
    }


    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(60 * 1000);
        mLocationRequest.setFastestInterval(15 * 1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mGoogleApiClient.connect();
    }

    public void hacer_zoom(View view) {


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocationUpdate();


    }

    private void requestLocationUpdate() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        else {
            try {
                CuantoZoom = mMap.getCameraPosition().zoom;
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                mMap.setMyLocationEnabled(true);
            }
            catch (Exception e){

            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onLocationChanged(Location location) {




        mMap.clear();
        milatitud=location.getLatitude();
        milongitud=location.getLongitude();
        mudar_marcador(milatitud,milongitud);
        cargar_puntos_mapa();
        mMap.moveCamera(CameraUpdateFactory.zoomBy(CuantoZoom));

        mMap.setMyLocationEnabled(true);
    }



    private void mudar_marcador(double milatitud, double milongitud) {
        ubicacion_dispositivo=new LatLng(milatitud,milongitud);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ubicacion_dispositivo, CuantoZoom));
        Bitmap bmp = BitmapFactory.decodeResource(
                getBaseContext().getResources(), R.drawable.localizacion_telf);
        Bitmap scaled = Bitmap.createScaledBitmap(bmp, 60, 80, false);
        mCurrLocationMarker=mMap.addMarker(new MarkerOptions()
                .title("Dispositivo de monitoreo")
                .draggable(false)
                .position(ubicacion_dispositivo)
                .icon(BitmapDescriptorFactory.fromBitmap(scaled)));
    }

    public void unidad_medida(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.kilometros:
                if (checked)
                    unidad_medida="K";
                break;
            case R.id.metros:
                if (checked)
                    unidad_medida="M";
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    public  void enviar(String mensaje) {
        PendingIntent piSent = PendingIntent.getBroadcast(getApplicationContext(),
                0, new Intent(SENT), 0);
        PendingIntent piDelivered = PendingIntent.getBroadcast(getApplicationContext(),
                0, new Intent(DELIVERED), 0);
        final BroadcastReceiver myReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
                unregisterReceiver(this);
            }
        };
        registerReceiver(myReceiver, new IntentFilter(SENT));
        //unregisterReceiver(myReceiver );

        //---when the SMS has been delivered---
        final BroadcastReceiver myReceiverDev = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
                unregisterReceiver(this);
            }
        };

        registerReceiver(myReceiverDev, new IntentFilter(DELIVERED));
        unregisterReceiver(myReceiverDev );
        SharedPreferences prefe = getSharedPreferences("Numero", Context.MODE_PRIVATE);
        numero_disp = prefe.getString("num", "nada");
        clave = prefe.getString("clave", "0000");


        String phoneNo = numero_disp;
        String sms = mensaje;

        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, sms, piSent, piDelivered);
            //smsManager.sendTextMessage(phoneNo, null, sms, null, null);
            /*Toast.makeText(getApplicationContext(), "SMS Sent!",
                    Toast.LENGTH_LONG).show();*/
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),
                    "" + e.getMessage(),
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
    public String calcular_distancia()
    {
        double distancia=0.0;
        Location ultimo_punto=buscar_ultimo_base_datos(true);
        Location celular=null;
        celular=busco_celular();
       /* Toast.makeText(getApplicationContext(), "El celular"+celular.toString(),
                Toast.LENGTH_LONG).show();*/

        if(celular!=null&&ultimo_punto!=null)
        {

            distancia=hacer_calculo_distancia(celular,ultimo_punto);
            pintar_ruta(celular,ultimo_punto);
            DecimalFormat df=new DecimalFormat("0.00");
            String formate = df.format(distancia);

        }
        DecimalFormat df=new DecimalFormat("0.00");
        if(distancia>=1000)
        { String formate = df.format(distancia/1000);
           return  formate+" Km";}
        else
            return df.format(distancia)+"m";
    }

    private void pintar_ruta(Location celular, Location ultimo_punto) {

    }

    public void calcular_distancia(View view)
    {

        if(view.getId()==R.id.distancia)
        {
        /*Location centro_zona=null;
        Location ultimo_punto=null;
        double distancia;
        centro_zona=buscar_ultimo_base_datos('Z');*/
            Location ultimo_punto=buscar_ultimo_base_datos(true);
            Location celular=null;
            celular=busco_celular();
       /* Toast.makeText(getApplicationContext(), "El celular"+celular.toString(),
                Toast.LENGTH_LONG).show();*/

            if(celular!=null&&ultimo_punto!=null)
            {

                double distancia=hacer_calculo_distancia(celular,ultimo_punto);
                DecimalFormat df=new DecimalFormat("0.00");
                String formate = df.format(distancia);
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("La distancia al dispositivo: "+formate+" mts.");
                alert.setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                alert.show();

                if (distancia>tam_zona)
                {
                    long[] pattern ={0,500,110};

                    if(vibrator.hasVibrator())
                        vibrator.vibrate(pattern,-1);
                    else {
                        Toast.makeText(getApplicationContext(), "Este telefono no puede vibrar..."+distancia+"mts",
                                Toast.LENGTH_LONG).show();
                    }

                }
            }
            else
            {
                Toast.makeText(getApplicationContext(), "No se tienen los puntos del centro de Zona y la ultima posición",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    private Location busco_celular() {
        // Log.i("mapa",""+mMap.getMyLocation().toString());
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Location myLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (myLocation == null) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_COARSE);
            String provider = lm.getBestProvider(criteria, true);
            myLocation = lm.getLastKnownLocation(provider);
        }
        return mMap.getMyLocation();
    }

    private float hacer_calculo_distancia(Location centro_zona, Location ultimo_punto) {
        float retorno=centro_zona.distanceTo(ultimo_punto);
        return retorno;
    }

    private Location buscar_ultimo_base_datos(boolean algo) {
        Cursor pun;
        MessagesDb todos=new MessagesDb(getBaseContext());
        Location devolver=new Location("");
//        String fecha = new SimpleDateFormat("YY-MM-dd").format(new Date());
        pun= todos.datos_puntos('M',1);
        if(pun!=null) {
            pun.moveToFirst();
            int donde;
            donde = pun.getColumnIndex("latitud");
            try {
                double latitud = pun.getDouble(donde);
                donde = pun.getColumnIndex("longitud");
                double longitud = pun.getDouble(donde);
                devolver.setLatitude(latitud);
                devolver.setLongitude(longitud);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        return  devolver;
    }
    private Location buscar_ultimo_base_datos(char que) {
        Cursor pun;
        MessagesDb todos=new MessagesDb(getBaseContext());
        Location devolver=new Location("");
        String fecha = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        pun= todos.datos_puntos(que,1);
        if(pun!=null)
        {  pun.moveToFirst();
            do{
                int donde;
                donde=pun.getColumnIndex("latitud");
                double latitud=pun.getDouble(donde);
                donde=pun.getColumnIndex("longitud");
                double longitud=pun.getDouble(donde);
                Toast.makeText(getApplicationContext(), "Cual punto:"+que+" latitud "+latitud+" longitud "+longitud,
                        Toast.LENGTH_LONG).show();
                if(que=='Z')
                {
                    donde=pun.getColumnIndex("radio");
                    if(donde!=-1)
                        tam_zona=pun.getDouble(donde);
                    else
                        tam_zona=0;
                }
                devolver.setLatitude(latitud);
                devolver.setLongitude(longitud);
                return devolver;
            }while(pun.moveToNext());
        }
        else
            return null;
    }
    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mGoogleApiClient.isConnected())
        {
            requestLocationUpdate();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

}