package com.botsydroid.controltarjeta;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import static android.widget.Spinner.MODE_DIALOG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ConfigFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ConfigFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConfigFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    Activity activity ;
    private String velocidad;

    private OnFragmentInteractionListener mListener;

    public ConfigFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ConfigFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConfigFragment newInstance(String param1, String param2) {
        ConfigFragment fragment = new ConfigFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final LinearLayout mRelativeLayout = (LinearLayout) inflater.inflate(R.layout.fragment_config,
                container, false);
        activity= getActivity();

        Button mButton_adicionales = (Button) mRelativeLayout.findViewById(R.id.config_bt_settins);
        mButton_adicionales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // here you set what you want to do when user clicks your button,

                if(activity instanceof MainActivity){
                    MainActivity myactivity = (MainActivity) activity;
                    myactivity.configuracion_adicionales();

                }

            }
        });
        Button mButton_alertas = (Button) mRelativeLayout.findViewById(R.id.config_bt_alertas);
        mButton_alertas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // here you set what you want to do when user clicks your button,

                if(activity instanceof MainActivity){
                    MainActivity myactivity = (MainActivity) activity;
                    myactivity.configuracion_alertas();

                }

            }
        });

        Button mButton4 = (Button) mRelativeLayout.findViewById(R.id.config_bt_monitor);
        mButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // here you set what you want to do when user clicks your button,
                if(activity instanceof MainActivity){
                    MainActivity myactivity = (MainActivity) activity;
                    myactivity.monitor();

                }

            }
        });

       /* Button mButton3 = (Button) mRelativeLayout.findViewById(R.id.config_bt_agregar);
        mButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // here you set what you want to do when user clicks your button,
                if(activity instanceof MainActivity){
                    MainActivity myactivity = (MainActivity) activity;
                    myactivity.Agregar_numero();
                }

            }
        });*/
        Button mButton45 = (Button) mRelativeLayout.findViewById(R.id.config_bt_monitor);
        mButton45.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(activity instanceof MainActivity) {
                    MainActivity myactivity = (MainActivity) activity;
                    myactivity.monitor();
                }


            }
        });
        Button mButton51 = (Button) mRelativeLayout.findViewById(R.id.config_bt_panic);
        mButton51.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(activity instanceof MainActivity){
                    MainActivity myactivity = (MainActivity) activity;
                    myactivity.enviar("Help me"+myactivity.getClave());
                }
            }
        });
        Button mButton23 = (Button) mRelativeLayout.findViewById(R.id.acitivar_salida_geozona);
        mButton23.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(activity instanceof MainActivity){
                    MainActivity myactivity = (MainActivity) activity;
                    ((MainActivity) activity).Activar_geofence();
                }
            }
        });

        Button mButton7 = (Button) mRelativeLayout.findViewById(R.id.config_bt_numeros_ayuda);
        mButton7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity myactivity = (MainActivity) activity;
                myactivity.Agregar_numero_ayuda();
            }
        });

        Button mButton6 = (Button) mRelativeLayout.findViewById(R.id.config_bt_localizacion);
        mButton6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // here you set what you want to do when user clicks your button,
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());

                alert.setTitle("Que deseas realizar");
                alert.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        LayoutInflater linf = LayoutInflater.from(getContext());
                        final View inflator = linf.inflate(R.layout.agregar_localizacion, null);
                        AlertDialog.Builder alert1 = new AlertDialog.Builder(getContext());
                        alert1.setTitle("Configuracion de Tiempo y frecuencia");
                        alert1.setView(inflator);
                        final EditText et1 = (EditText) inflator.findViewById(R.id.cuanto_tiempo);
                        final EditText et2 = (EditText) inflator.findViewById(R.id.cuantas_veces);
                        alert1.setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                int tiempo = Integer.parseInt(et1.getText().toString());
                                int veces =Integer.parseInt(et2.getText().toString());
                                if(tiempo<255&&veces<255)
                                {
                                    MainActivity myactivity = (MainActivity) activity;
                                    myactivity.enviar("fix" + String.format("%3s", tiempo).replace(' ', '0')+"m"+String.format("%3s",veces).replace(' ','0')+myactivity.getClave());
                                    myactivity.showProgress(true);
                                }
                                else
                                {
                                    Snackbar snackbar = Snackbar
                                            .make(mRelativeLayout, "Debe ser menor 255 ", Snackbar.LENGTH_LONG);

                                    snackbar.show();
                                }

                            }
                        });

                        alert1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        });

                        alert1.show();
                    }
                });

                alert.setNegativeButton("Desactivar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MainActivity myactivity = (MainActivity) activity;
                        myactivity.enviar("Nospeed" + myactivity.getClave());
                        myactivity.showProgress(true);
                    }
                });
                alert.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });
                alert.show();
            }
        });

        return mRelativeLayout;
    }
    public Dialog callDialog(String titulo, CharSequence[] opciones, final String cual)
    {
        final Object[] checkedItem = new Object[1];
        //Initialize the Alert Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//Source of the data in the DIalog


// Set the dialog title
        builder.setTitle(titulo)
// Specify the list array, the items to be selected by default (null for none),
// and the listener through which to receive callbacks when items are selected
                .setSingleChoiceItems(opciones, -1, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
// TODO Auto-generated method stub

                    }
                })

// Set the action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
// User clicked OK, so save the result somewhere
// or return them to the component that opened the dialog
                        ListView lw = ((AlertDialog)dialog).getListView();
                        if(cual.equals("sensibilidad")){
                            MainActivity myactivity = (MainActivity) activity;
                            myactivity.sensibilidad(lw.getCheckedItemPosition()+1);

                        }
                        if(cual.equals("frecuencia")){
                            MainActivity myActivity=(MainActivity) activity;
                            myActivity.frecuencia( lw.getCheckedItemPosition()+1);
                        }

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        return builder.create();

    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
