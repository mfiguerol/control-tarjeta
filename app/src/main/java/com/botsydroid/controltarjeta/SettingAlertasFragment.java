package com.botsydroid.controltarjeta;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SettingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingAlertasFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    Activity activity ;

    private OnFragmentInteractionListener mListener;

    public SettingAlertasFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SettingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingAlertasFragment newInstance(String param1, String param2) {
        SettingAlertasFragment fragment = new SettingAlertasFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final LinearLayout mRelativeLayout = (LinearLayout) inflater.inflate(R.layout.fragment_config_alertas,
                container, false);
        activity= getActivity();
        Button mButton5 = (Button) mRelativeLayout.findViewById(R.id.config_bt_velocidad);
        mButton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // here you set what you want to do when user clicks your button,
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());

                alert.setTitle("Que deseas realizar");
                alert.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        LayoutInflater linf = LayoutInflater.from(getContext());
                        final View inflator = linf.inflate(R.layout.agregar_velocidad, null);
                        AlertDialog.Builder alert1 = new AlertDialog.Builder(getContext());
                        alert1.setTitle("");
                        alert1.setView(inflator);
                        final EditText et1 = (EditText) inflator.findViewById(R.id.etVelocidad);
                        alert1.setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String velocidad = et1.getText().toString();

                                if(Integer.parseInt(velocidad)>30)

                                {
                                    MainActivity myactivity = (MainActivity) activity;
                                    myactivity.enviar("speed" + myactivity.getClave() + " "+String.format("%3s", velocidad).replace(' ', '0'));
                                    myactivity.showProgress(true);
                                }
                                else
                                {
                                    Snackbar snackbar = Snackbar
                                            .make(mRelativeLayout, "Debe ser mayor a 30", Snackbar.LENGTH_LONG);

                                    snackbar.show();
                                }

                            }
                        });

                        alert1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        });

                        alert1.show();
                    }
                });

                alert.setNegativeButton("Desactivar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MainActivity myactivity = (MainActivity) activity;
                        myactivity.enviar("Nospeed" + myactivity.getClave());
                        myactivity.showProgress(true);
                    }
                });
                alert.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });
                alert.show();
            }
        });

        Button mButtontemp = (Button) mRelativeLayout.findViewById(R.id.config_bt_temperatura);
        mButtontemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // here you set what you want to do when user clicks your button,
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Que deseas realizar");
                alert.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        LayoutInflater linf = LayoutInflater.from(getContext());
                        final View inflator = linf.inflate(R.layout.agregar_temperatura, null);
                        AlertDialog.Builder alert1 = new AlertDialog.Builder(getContext());
                        alert1.setTitle("");
                        alert1.setView(inflator);
                        final EditText et1 = (EditText) inflator.findViewById(R.id.etTemperatura1);
                        final EditText et2 = (EditText) inflator.findViewById(R.id.etTemperatura2);
                        alert1.setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String valor1 = et1.getText().toString();
                                String valor2 = et2.getText().toString();

                                if(Integer.parseInt(valor1)>-50 && Integer.parseInt(valor1)<125)

                                {
                                    if(Integer.parseInt(valor1)<0) {
                                        if (Math.abs(Integer.parseInt(valor1)) < 10) {
                                            valor1 = String.format("%3s", valor1).replace("-", "-00");
                                        }
                                        else
                                        {
                                            valor1=String.format("%3s",valor1).replace("-","-0");
                                        }
                                    }
                                    else
                                    {
                                        valor1=String.format("%3s",valor1).replace(" ","0");
                                    }

                                    if(Integer.parseInt(valor2)>-50 && Integer.parseInt(valor2)<125)
                                    {
                                        if(Integer.parseInt(valor2)<0) {
                                            if (Math.abs(Integer.parseInt(valor2)) < 10) {
                                                valor2 = String.format("%3s", valor1).replace("-", "-00");
                                            }
                                            else
                                            {
                                                valor2=String.format("%3s",valor2).replace("-","-0");
                                            }
                                        }
                                        else
                                        {
                                            valor2=String.format("%3s",valor2).replace(" ","0");
                                        }
                                        MainActivity myactivity = (MainActivity) activity;
                                        myactivity.enviar("temperature" + myactivity.getClave() + " "+valor1+ "C,"+valor2 +"C");
                                        myactivity.showProgress(true);
                                    }
                                    else
                                    {
                                        Snackbar snackbar = Snackbar
                                                .make(mRelativeLayout, "Debe ser menor 125 y ,mayor a -50", Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                    }
                                }
                                else
                                {
                                    Snackbar snackbar = Snackbar
                                            .make(mRelativeLayout, "Debe ser menor 125 y ,mayor a -50", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                }

                            }
                        });

                        alert1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        });

                        alert1.show();
                    }
                });

                alert.setNegativeButton("Desactivar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MainActivity myactivity = (MainActivity) activity;
                        myactivity.enviar("Notemperature" + myactivity.getClave());
                        myactivity.showProgress(true);
                    }
                });
                alert.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });
                alert.show();
            }
        });
        Button mButtonbateria = (Button) mRelativeLayout.findViewById(R.id.config_bt_bateria);
        mButtonbateria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // here you set what you want to do when user clicks your button,
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Que deseas realizar");
                alert.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        LayoutInflater linf = LayoutInflater.from(getContext());
                         MainActivity myactivity = (MainActivity) activity;
                                        myactivity.enviar("lowbattery" +myactivity.getClave() + " on");
                                        myactivity.showProgress(true);
                    }
                });
                alert.setNegativeButton("Desactivar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MainActivity myactivity = (MainActivity) activity;
                        myactivity.enviar("lowbattery" +myactivity.getClave() + " off");
                        myactivity.showProgress(true);
                    }
                });
                        alert.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                alert.show();
            }
        });
        Button mButtoncombustible = (Button) mRelativeLayout.findViewById(R.id.config_bt_combustible);
        mButtoncombustible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // here you set what you want to do when user clicks your button,
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Que deseas realizar");
                alert.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
                        LayoutInflater linf = LayoutInflater.from(getContext());
                        AlertDialog.Builder alert1 = new AlertDialog.Builder(getContext());
                            public void onClick(DialogInterface dialog, int whichButton) {
                                MainActivity myactivity = (MainActivity) activity;
                                myactivity.enviar("oil" +myactivity.getClave()+" on");
                                myactivity.showProgress(true);
                    }
                });
                alert.setNegativeButton("Desactivar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MainActivity myactivity = (MainActivity) activity;
                        myactivity.enviar("oil" +myactivity.getClave()+" off");
                        myactivity.showProgress(true);
                    }
                });
                alert.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                alert.show();
            }
        });
        Button mButtonignicion = (Button) mRelativeLayout.findViewById(R.id.config_bt_ignicion);
        mButtonignicion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // here you set what you want to do when user clicks your button,
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Que deseas realizar");
                alert.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                MainActivity myactivity = (MainActivity) activity;
                                myactivity.enviar("acc" +myactivity.getClave());
                                myactivity.showProgress(true);}
                });
                alert.setNegativeButton("Desactivar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MainActivity myactivity = (MainActivity) activity;
                        myactivity.enviar("noacc" +myactivity.getClave());
                        myactivity.showProgress(true);
                    }
                });
                alert.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                alert.show();
            }
        });
        Button mmovi = (Button) mRelativeLayout.findViewById(R.id.config_bt_movimiento);
        mmovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // here you set what you want to do when user clicks your button,
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());

                alert.setTitle("Que deseas realizar");
                alert.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        LayoutInflater linf = LayoutInflater.from(getContext());
                        final View inflator = linf.inflate(R.layout.agregar_movimiento, null);
                        AlertDialog.Builder alert1 = new AlertDialog.Builder(getContext());
                        alert1.setTitle("");
                        alert1.setView(inflator);
                        final EditText et1 = (EditText) inflator.findViewById(R.id.etMovimiento);
                        alert1.setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String velocidad = et1.getText().toString();
                                MainActivity myactivity = (MainActivity) activity;
                                myactivity.enviar("move" + myactivity.getClave() + " "+String.format("%4s", velocidad).replace(' ', '0'));
                                myactivity.showProgress(true);

                            }
                        });

                        alert1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        });
                        alert1.show();
                    }
                });

                alert.setNegativeButton("Desactivar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MainActivity myactivity = (MainActivity) activity;
                        myactivity.enviar("Nomove" + myactivity.getClave());
                        myactivity.showProgress(true);
                    }
                });
                alert.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });
                alert.show();
            }
        });
        Button mButtonnpgps = (Button) mRelativeLayout.findViewById(R.id.config_bt_npgps);
        mButtonnpgps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // here you set what you want to do when user clicks your button,
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Que deseas realizar");
                alert.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                                MainActivity myactivity = (MainActivity) activity;
                                myactivity.enviar("gpssignal" +myactivity.getClave()+ " on");
                                myactivity.showProgress(true);
                    }
                });
                alert.setNegativeButton("Desactivar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MainActivity myactivity = (MainActivity) activity;
                        myactivity.enviar("gpssignal" +myactivity.getClave()+" off");
                        myactivity.showProgress(true);
                    }
                });
                alert.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                alert.show();
            }
        });
        return mRelativeLayout;
    }
    public Dialog  callDialog(String titulo, CharSequence[] opciones, final String cual)
    {
        final Object[] checkedItem = new Object[1];
        //Initialize the Alert Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//Source of the data in the DIalog


// Set the dialog title
        builder.setTitle(titulo)
// Specify the list array, the items to be selected by default (null for none),
// and the listener through which to receive callbacks when items are selected
                .setSingleChoiceItems(opciones, -1, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
// TODO Auto-generated method stub

                    }
                })

// Set the action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
// User clicked OK, so save the result somewhere
// or return them to the component that opened the dialog
                        ListView lw = ((AlertDialog)dialog).getListView();
                        if(cual.equals("sensibilidad")){
                            MainActivity myactivity = (MainActivity) activity;
                            myactivity.sensibilidad(lw.getCheckedItemPosition()+1);

                        }
                        if(cual.equals("frecuencia")){
                            MainActivity myActivity=(MainActivity) activity;
                            myActivity.frecuencia( lw.getCheckedItemPosition()+1);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        return builder.create();

    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
