package com.botsydroid.controltarjeta;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.*;

import static android.content.Context.AUDIO_SERVICE;
import static android.content.Context.VIBRATOR_SERVICE;
import android.app.ProgressDialog;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Principal.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Principal#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class Principal extends Fragment {
  // TODO: Rename parameter arguments, choose names that match
  // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";

  // TODO: Rename and change types of parameters
  private String mParam1;
  private String mParam2;
  private String clave;
  private String velocidad;
  private String encendido;
  private String accion;
  ProgressDialog progressDialog;
  // ImageButton start;
  ImageButton startBt;
  Button lock;
  Button unlock;
  Button panic;
  Button lockSilent;
  MainActivity myactivity;


  private SoundPool soundPool;
  private int soundID;
  boolean plays = false, loaded = false;
  float actVolume, maxVolume, volume;
  AudioManager audioManager;
  Activity activity;

  private OnFragmentInteractionListener mListener;

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @param param1 Parameter 1.
   * @param param2 Parameter 2.
   * @return A new instance of fragment Principal.
   */
  // TODO: Rename and change types and number of parameters
  public static Principal newInstance(String param1, String param2) {
    Principal fragment = new Principal();
    Bundle args = new Bundle();
    args.putString(ARG_PARAM1, param1);
    args.putString(ARG_PARAM2, param2);

    fragment.setArguments(args);
    return fragment;
  }
  public Principal() {
    // Required empty public constructor
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      mParam1 = getArguments().getString(ARG_PARAM1);
      mParam2 = getArguments().getString(ARG_PARAM2);
    }

  }
  @Override
  public void onDestroy() {
    super.onDestroy();
    unloadAll();

  }
  public void unloadAll() {
    if (soundPool!=null) {
      soundPool.release();
    }
  }
  public int load() {
    soundPool.load(getContext(), R.raw.chulito, 1);
    return 1;

  }
  private void mostrar_progress() {
    progressDialog = new ProgressDialog(getContext());
    progressDialog.setMessage("Enviando..."); // Setting Message
    progressDialog.setTitle("Control Tarjeta"); // Setting Title
    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
    progressDialog.show();
    progressDialog.setCancelable(true);
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          Thread.sleep(500);
        } catch (Exception e) {
          e.printStackTrace();
        }
        progressDialog.dismiss();
      }
    }).start();


  }
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    activity= getActivity();
    audioManager = (AudioManager) getContext().getSystemService(AUDIO_SERVICE);
    actVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
    maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    //volume = actVolume / maxVolume;
    volume=maxVolume;
    soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);

    soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
      @Override
      public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
        loaded = true;
      }
    });
    load();
    final ConstraintLayout mRelativeLayout = (ConstraintLayout) inflater.inflate(R.layout.fragment_principal,
            container, false);
    //mLoginFormView = mRelativeLayout.findViewById(R.id.login_form);
    //mProgressView = mRelativeLayout.findViewById(R.id.login_progress);
    Bundle args = getArguments();
    clave = args.getString("clave", "0000");
    accion=args.getString("accion","nada");

    if(clave=="0000")
    {
      MainActivity myactivity = (MainActivity) activity;
      clave=myactivity.getClave();
    }
    if(startBt==null)
    startBt =(ImageButton)mRelativeLayout.findViewById((R.id.ibStart));
    if(lock==null)
    lock=(Button)mRelativeLayout.findViewById((R.id.lockon));
    if(unlock==null)
    unlock=(Button)mRelativeLayout.findViewById((R.id.lockoff));
    lockSilent=(Button)mRelativeLayout.findViewById((R.id.silent));
    panic=(Button)mRelativeLayout.findViewById((R.id.sos));
    myactivity = (MainActivity) activity;
//    startBt.setPadding(-1,-1,-1,-1);
    float val =2.5f;
    startBt.setScaleX(val);
    startBt.setScaleY(val);

    if(accion.equalsIgnoreCase("armar_alarma"))
    {

      lock.setBackgroundResource(R.drawable.lock_activo);
      unlock.setBackgroundResource(R.drawable.unlock_inactivo);
      lockSilent.setBackgroundResource(R.drawable.lock_sonido_inactivo);
        String cadena=myactivity.boton_central();

        switch (cadena)
        {
            case "on":startBt.setBackgroundResource(R.drawable.boton_central_verde);
                Toast.makeText(getContext(), "boton central verde!",
                        Toast.LENGTH_LONG).show();
                break;
            case "off":startBt.setBackgroundResource(R.drawable.boton_central_rojo);
                Toast.makeText(getContext(), "boton central rojo!",
                        Toast.LENGTH_LONG).show();
                break;
        }

    }
    if(accion.equalsIgnoreCase("desarmar_alarma"))
    {

      lock.setBackgroundResource(R.drawable.lock_inactivo);
      unlock.setBackgroundResource(R.drawable.unlock_activo);
      lockSilent.setBackgroundResource(R.drawable.lock_sonido_inactivo);
        String cadena=myactivity.boton_central();

        switch (cadena)
        {
            case "on":startBt.setBackgroundResource(R.drawable.boton_central_verde);
                Toast.makeText(getContext(), "boton central verde!",
                        Toast.LENGTH_LONG).show();
                break;
            case "off":startBt.setBackgroundResource(R.drawable.boton_central_rojo);
                Toast.makeText(getContext(), "boton central rojo!",
                        Toast.LENGTH_LONG).show();
                break;
        }


    }
    if(accion.equalsIgnoreCase("silente"))
    {

      lock.setBackgroundResource(R.drawable.lock_inactivo);
      unlock.setBackgroundResource(R.drawable.unlock_inactivo);
      lockSilent.setBackgroundResource(R.drawable.lock_sonido_activ);
        String cadena=myactivity.boton_central();
        switch (cadena)
        {
            case "on":startBt.setBackgroundResource(R.drawable.boton_central_verde);
                Toast.makeText(getContext(), "boton central verde!",
                        Toast.LENGTH_LONG).show();
                break;
            case "off":startBt.setBackgroundResource(R.drawable.boton_central_rojo);
                Toast.makeText(getContext(), "boton central rojo!",
                        Toast.LENGTH_LONG).show();
                break;
        }

    }
    if(accion.equalsIgnoreCase("encender_motor"))
    {

      startBt.setBackgroundResource(R.drawable.boton_central_verde);
        String cadena=myactivity.boton_lock();
        switch (cadena)
        {
            case "boton_unlock":
                lock.setBackgroundResource(R.drawable.lock_activo);
                unlock.setBackgroundResource(R.drawable.unlock_inactivo);
                lockSilent.setBackgroundResource(R.drawable.lock_sonido_inactivo);
                break;
            case "boton_lock":lock.setBackgroundResource(R.drawable.lock_inactivo);
                unlock.setBackgroundResource(R.drawable.unlock_activo);
                lockSilent.setBackgroundResource(R.drawable.lock_sonido_inactivo);
                break;
            case "boton_lock_silent":
                lock.setBackgroundResource(R.drawable.lock_inactivo);
                unlock.setBackgroundResource(R.drawable.unlock_inactivo);
                lockSilent.setBackgroundResource(R.drawable.lock_sonido_activ);
                break;

        }

    }
    if(accion.equalsIgnoreCase("apagar_motor"))
    {

      startBt.setBackgroundResource(R.drawable.boton_central_rojo);

    }

    if(clave=="0000")
    {
      clave=myactivity.getClave();
    }
    lock.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Que deseas realizar");
        alert.setPositiveButton("Armar", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
            MainActivity myactivity = (MainActivity) activity;
            myactivity.enviar("Arm"+clave);
            myactivity.showProgress(true);
          }
        });
        alert.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
          }
        });
        alert.show();

        mostrar_progress();
        vibrar();
      }
    });
    unlock.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Que deseas realizar");
        alert.setPositiveButton("Desarmar", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
            MainActivity myactivity = (MainActivity) activity;
            myactivity.enviar("Disarm"+clave);
            myactivity.showProgress(true);
          }
        });
        alert.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
          }
        });
        alert.show();
        vibrar();
        mostrar_progress();
      }
    });
    panic.setOnLongClickListener(new OnLongClickListener() {

      @Override
      public boolean onLongClick(View v) {

        Cursor pun=null;
        MessagesDb todos=new MessagesDb(getContext());
        pun=todos.buscar_numeros_emergengia(myactivity.numero_disp);
        pun.moveToFirst();
        while ( !pun.isAfterLast()) {
          String numero_a_enviar= pun.getString(pun.getColumnIndex("numero_ayuda"));
          pun.moveToNext();
          myactivity.enviar("Mallku Cars le informa que ha sido activado el sistema de auxilio desde el número "+myactivity.numero_disp+".", numero_a_enviar);
        }

        vibrar();
        panic.setBackgroundResource(R.drawable.sos_inactivo);
        panic.setEnabled(false);
        mostrar_progress();
        unlock.setBackgroundResource(R.drawable.unlock_activo);
        unlock.setEnabled(true);
        mostrar_progress();
      return false;
      }
    });
    lockSilent.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Que deseas realizar");
        alert.setPositiveButton("Armar en Silencio", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
            MainActivity myactivity = (MainActivity) activity;
            myactivity.enviar("Silent"+clave);
            myactivity.showProgress(true);
          }
        });
        alert.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
          }
        });
        alert.show();
        vibrar();
        mostrar_progress();
      }
    });
    startBt.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        vibrar();
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Que deseas realizar");
        alert.setPositiveButton("Habilitar", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
            MainActivity myactivity = (MainActivity) activity;
            myactivity.enviar("Resume" + clave);
            myactivity.showProgress(true);
          }
        });
        alert.setNegativeButton("Inmovilizar", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
            MainActivity myactivity = (MainActivity) activity;
            myactivity.enviar("Stop" + clave);
            myactivity.showProgress(true);
          }
        });
        alert.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
          }
        });
        alert.show();
      }
    });


    /*args.putString("imagen_boton_central",imagen_boton_central);
    args.putString("imagen_boton_lock",imagen_boton_lock);
    args.putString("imagen_boton_unlock",imagen_boton_unlock);
    args.putString("imagen_boton_silent",imagen_boton_silent);*/
    return mRelativeLayout;
  }


  private void vibrar() {
    Vibrator vibrator = (Vibrator) getContext().getSystemService(VIBRATOR_SERVICE);
    vibrator.vibrate(60);
    soundPool.play(1, volume, volume, 1, 0, 1f);

  }

  // TODO: Rename method, update argument and hook method into UI event
  public void onButtonPressed(Uri uri) {
    if (mListener != null) {
      mListener.onFragmentInteraction(uri);
    }
  }
  @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)


  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(context.toString()
              + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   * <p>
   * See the Android Training lesson <a href=
   * "http://developer.android.com/training/basics/fragments/communicating.html"
   * >Communicating with Other Fragments</a> for more information.
   */
  public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    void onFragmentInteraction(Uri uri);
  }


}