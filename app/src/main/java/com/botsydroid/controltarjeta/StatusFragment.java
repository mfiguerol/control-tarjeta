package com.botsydroid.controltarjeta;

import android.content.Context;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StatusFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StatusFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StatusFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
private String mensaje[];
    private OnFragmentInteractionListener mListener;

    public StatusFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment StatusFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StatusFragment newInstance(String param1, String param2) {
        StatusFragment fragment = new StatusFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_status, container, false);
        String mensaje = getArguments().getString("mensaje");
        String[] valores;
        TextView texto_combustible=(TextView) view.findViewById(R.id.oil_texto);
        String[] varios = mensaje.split("\n");
        if(mensaje.contains("ODO:"))
        {
            valores = varios[0].split(":");
            if (valores[0].equalsIgnoreCase("POWER")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.power);
                valores[1] = valores[1].trim();
                if (valores[1].equalsIgnoreCase("ON")) {
                    auxiliar.setImageResource(R.drawable.energia_activo_bajo);
                    Log.i("imagen verde", valores[1]);
                } else {
                    auxiliar.setImageResource(R.drawable.energia_desactivado_bajo);
                    Log.i("imagen gris", valores[1]);
                }
            }
            valores = varios[1].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("Bat")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.bateria);
                TextView porcen = (TextView) view.findViewById(R.id.texto_bateria);
                String valor = valores[1].replace("%", "");
                long porcentaje = Math.round(Double.parseDouble(valor));
                porcen.setText(String.valueOf(porcentaje) + "%");
                porcen.setTextColor(Color.parseColor("#ffffff"));
                Log.i("porcentaje", valor);
                if (porcentaje <= 25) {
                    auxiliar.setImageResource(R.drawable.bateria_rojo);
                }
                if (porcentaje < 75 && porcentaje > 25) {
                    auxiliar.setImageResource(R.drawable.bateria_amarillo);
                }
                if (porcentaje >= 75) {
                    auxiliar.setImageResource(R.drawable.bateria_verde);
                }
            }
            valores = varios[2].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("GPRS")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.gprs);
                valores[1] = valores[1].trim();
                if (valores[1].equalsIgnoreCase("ON")) {
                    auxiliar.setImageResource(R.drawable.gprsr_activo_bajo);
                } else auxiliar.setImageResource(R.drawable.gprsr_inactivo_bajo);
            }
            valores = varios[3].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("GPS")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.gps);
                valores[1] = valores[1].trim();
                if (valores[1].equalsIgnoreCase("ON")) {
                    auxiliar.setImageResource(R.drawable.gps_activo_bajo);
                } else auxiliar.setImageResource(R.drawable.gps_inactivo_bajo);
            }
            valores = varios[4].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("ACC")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.ignicion);
                valores[1] = valores[1].trim();
                if (valores[1].equalsIgnoreCase("ON")) {
                    auxiliar.setImageResource(R.drawable.ignicion_activo);
                } else auxiliar.setImageResource(R.drawable.ignicion_inactivo);
            }
            valores = varios[5].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("Door")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.puertas);
                valores[1] = valores[1].trim();

                if (valores[1].equalsIgnoreCase("OFF")) {
                    auxiliar.setBackgroundResource(R.drawable.fondoprincialstatuscerrado);


                } else {
                    auxiliar.setBackgroundResource(R.drawable.fondoprincialstatusabierto);


                }
            }
            valores = varios[6].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("GSM")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.gsm);
                long porcentaje = Math.round(Double.parseDouble(valores[1]) * 3.125);
                TextView porcen = (TextView) view.findViewById(R.id.gsm_texto);
                porcen.setTextColor(Color.parseColor("#ffffff"));
                porcen.setText(String.valueOf(porcentaje) + "%");
                if (porcentaje <= 25) {
                    auxiliar.setImageResource(R.drawable.gsm_rojo_bajo);
                }
                if (porcentaje < 75 && porcentaje > 25) {
                    auxiliar.setImageResource(R.drawable.gsm_amarillo_bajo);
                }
                if (porcentaje >= 75) {
                    auxiliar.setImageResource(R.drawable.gsm_verde_bajo);
                }
            }
            valores = varios[7].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("OIL")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.combustible);
                TextView porcen = (TextView) view.findViewById(R.id.oil_texto);
                String valor = valores[1].replace("%", "");
                long porcentaje = Math.round(Double.parseDouble(valor));
                porcen.setText(String.valueOf(porcentaje) + "%");
                porcen.setVisibility(View.VISIBLE);
                porcen.setTextColor(Color.parseColor("#ffffff"));
                Log.i("porcentaje", valor);
                if (porcentaje <= 25) {
                    auxiliar.setImageResource(R.drawable.combustible_rojo);
                }
                if (porcentaje < 75 && porcentaje > 25) {
                    auxiliar.setImageResource(R.drawable.combustible_amarillo);
                }
                if (porcentaje >= 75) {
                    auxiliar.setImageResource(R.drawable.combustible_verde);
                }
            }
            valores = varios[8].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("ODO")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.odometro);
                TextView porcen = (TextView) view.findViewById(R.id.texto_odometro);
                String valor = valores[1].replace("%", "");
                double porcentaje = Double.parseDouble(valor);
                porcen.setText(String.valueOf(porcentaje));
                porcen.setVisibility(View.VISIBLE);
                porcen.setTextColor(Color.parseColor("#ffffff"));
                Log.i("porcentaje", valor);

            }
            valores = varios[11].split(":");
            if (valores[0].equalsIgnoreCase("ARM")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.candado);
                valores[1] = valores[1].trim();
                if (valores[1].equalsIgnoreCase("ON")) {
                    auxiliar.setImageResource(R.drawable.estatus_armado);
                } else
                    auxiliar.setImageResource(R.drawable.estatus_desarmado);

            }
        }//para los modelos de placa TK-105B
        else
        {
            ImageView odometro= (ImageView) view.findViewById(R.id.odometro);
            odometro.setImageResource(R.drawable.odometro_inactivo);
            TextView recorrido = (TextView) view.findViewById(R.id.texto_odometro);
            recorrido.setText("XXX");
            recorrido.setTextColor(Color.parseColor("#ffffff"));
            valores = varios[0].split(":");
            if (valores[0].equalsIgnoreCase("POWER")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.power);
                valores[1] = valores[1].trim();
                if (valores[1].equalsIgnoreCase("ON")) {
                    auxiliar.setImageResource(R.drawable.energia_activo_bajo);
                    Log.i("imagen verde", valores[1]);
                } else {
                    auxiliar.setImageResource(R.drawable.energia_desactivado_bajo);
                    Log.i("imagen gris", valores[1]);
                }
            }
            valores = varios[1].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("Bat")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.bateria);
                TextView porcen = (TextView) view.findViewById(R.id.texto_bateria);
                String valor = valores[1].replace("%", "");
                long porcentaje = Math.round(Double.parseDouble(valor));
                porcen.setText(String.valueOf(porcentaje) + "%");
                porcen.setTextColor(Color.parseColor("#ffffff"));
                Log.i("porcentaje", valor);
                if (porcentaje <= 25) {
                    auxiliar.setImageResource(R.drawable.bateria_rojo);
                }
                if (porcentaje < 75 && porcentaje > 25) {
                    auxiliar.setImageResource(R.drawable.bateria_amarillo);
                }
                if (porcentaje >= 75) {
                    auxiliar.setImageResource(R.drawable.bateria_verde);
                }
            }
            valores = varios[2].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("GPRS")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.gprs);
                valores[1] = valores[1].trim();
                if (valores[1].equalsIgnoreCase("ON")) {
                    auxiliar.setImageResource(R.drawable.gprsr_activo_bajo);
                } else auxiliar.setImageResource(R.drawable.gprsr_inactivo_bajo);
            }
            valores = varios[3].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("GPS")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.gps);
                valores[1] = valores[1].trim();
                if (valores[1].equalsIgnoreCase("ON")) {
                    auxiliar.setImageResource(R.drawable.gps_activo_bajo);
                } else auxiliar.setImageResource(R.drawable.gps_inactivo_bajo);
            }
            valores = varios[4].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("ACC")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.ignicion);
                valores[1] = valores[1].trim();
                if (valores[1].equalsIgnoreCase("ON")) {
                    auxiliar.setImageResource(R.drawable.ignicion_activo);
                } else auxiliar.setImageResource(R.drawable.ignicion_inactivo);
            }
            valores = varios[5].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("Door")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.puertas);
                valores[1] = valores[1].trim();

                if (valores[1].equalsIgnoreCase("OFF")) {
                    auxiliar.setBackgroundResource(R.drawable.fondoprincialstatuscerrado);


                } else {
                    auxiliar.setBackgroundResource(R.drawable.fondoprincialstatusabierto);


                }
            }
            valores = varios[6].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("GSM")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.gsm);
                long porcentaje = Math.round(Double.parseDouble(valores[1]) * 3.125);
                TextView porcen = (TextView) view.findViewById(R.id.gsm_texto);
                porcen.setTextColor(Color.parseColor("#ffffff"));
                porcen.setText(String.valueOf(porcentaje) + "%");
                if (porcentaje <= 25) {
                    auxiliar.setImageResource(R.drawable.gsm_rojo_bajo);
                }
                if (porcentaje < 75 && porcentaje > 25) {
                    auxiliar.setImageResource(R.drawable.gsm_amarillo_bajo);
                }
                if (porcentaje >= 75) {
                    auxiliar.setImageResource(R.drawable.gsm_verde_bajo);
                }
            }
            valores = varios[7].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("OIL")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.combustible);
                TextView porcen = (TextView) view.findViewById(R.id.oil_texto);
                String valor = valores[1].replace("%", "");
                long porcentaje = Math.round(Double.parseDouble(valor));
                porcen.setText(String.valueOf(porcentaje) + "%");
                porcen.setVisibility(View.VISIBLE);
                porcen.setTextColor(Color.parseColor("#ffffff"));
                Log.i("porcentaje", valor);
                if (porcentaje <= 25) {
                    auxiliar.setImageResource(R.drawable.combustible_rojo);
                }
                if (porcentaje < 75 && porcentaje > 25) {
                    auxiliar.setImageResource(R.drawable.combustible_amarillo);
                }
                if (porcentaje >= 75) {
                    auxiliar.setImageResource(R.drawable.combustible_verde);
                }
            }
            else //no reporta combustible
            {
                ImageView combustible= (ImageView) view.findViewById(R.id.combustible);
                combustible.setImageResource(R.drawable.combustible_gris);
                recorrido = (TextView) view.findViewById(R.id.oil_texto);
                recorrido.setText("XXX");
                recorrido.setTextColor(Color.parseColor("#ffffff"));

            }
            /*valores = varios[8].split(":");
            valores[0] = valores[0].trim();
            if (valores[0].equalsIgnoreCase("ODO")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.odometro);
                TextView porcen = (TextView) view.findViewById(R.id.texto_odometro);
                String valor = valores[1].replace("%", "");
                double porcentaje = Double.parseDouble(valor);
                porcen.setText(String.valueOf(porcentaje));
                porcen.setVisibility(View.VISIBLE);
                porcen.setTextColor(Color.parseColor("#ffffff"));
                Log.i("porcentaje", valor);

            }*/
            valores = varios[9].split(":");
            if (valores[0].equalsIgnoreCase("ARM")) {
                ImageView auxiliar = (ImageView) view.findViewById(R.id.candado);
                valores[1] = valores[1].trim();
                if (valores[1].equalsIgnoreCase("ON")) {
                    auxiliar.setImageResource(R.drawable.estatus_armado);
                } else
                    auxiliar.setImageResource(R.drawable.estatus_desarmado);

            }
        }


        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
