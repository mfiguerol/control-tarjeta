package com.botsydroid.controltarjeta;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CreatePasswordActivity extends AppCompatActivity {
    TextView pass1,pass2;
    Button boton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_password);
        pass1=(EditText) findViewById(R.id.password1);
        pass2=(EditText) findViewById(R.id.password2);
        boton=(Button) findViewById(R.id.Aceptar);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text1,text2;
                text1=pass1.getText().toString();
                text2=pass2.getText().toString();
                if(text1.equals("")||text2.equals(""))
                {
                    Toast.makeText(CreatePasswordActivity.this,"No pueden estar vacios los password", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if(text1.equals(text2))
                    {
                        SharedPreferences settings=getSharedPreferences("PREFS",0);
                        SharedPreferences.Editor editor=settings.edit();
                        editor.putString("password_mallku",text1);
                        editor.apply();
                        Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        Toast.makeText(CreatePasswordActivity.this,"No coinciden los password", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }
}
