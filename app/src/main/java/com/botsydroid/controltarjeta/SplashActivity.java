package com.botsydroid.controltarjeta;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    String password_mallku;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        SharedPreferences settings=getSharedPreferences("PREFS",0);
        password_mallku=settings.getString("password_mallku","");


        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(password_mallku.equals(""))
                {
                Intent intent=new Intent(getApplicationContext(),CreatePasswordActivity.class);
                startActivity(intent);
                finish();
                }
                else
                {
                    Intent intent=new Intent(getApplicationContext(),ColocarPasswordActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        },2000);
    }
}
