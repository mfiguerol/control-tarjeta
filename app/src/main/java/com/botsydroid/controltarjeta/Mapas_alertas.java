package com.botsydroid.controltarjeta;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.*;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Mapas_alertas extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener{

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation, ultimo = null,ultima_zona=null;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    private FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;
    boolean zona_circular = false;
    boolean zona_rectangular=false;
    double tam_zona;
    private double milatitud;
    private double milongitud;
    private LatLng ubicacion_dispositivo;
    private float CuantoZoom=1;
    private Vibrator vibrator;
    private  List<LatLng> poligono=new ArrayList<LatLng>();
    private static String SENT = "SMS_SENT";
    private static String DELIVERED = "SMS_DELIVERED";
    private static int MAX_SMS_MESSAGE_LENGTH = 160;
    private String Speed;
    String numero_disp;
    String clave;
    String tipos_zonas[] = {"Seleccione","Zona Rectangular"};
    private Spinner spinner;
    private String unidad_medida;
    private String tipo_alarma;
    private String completo;
    private String fecha;
    private String latitud;
    private String longitud;
    private String tipo_alerta;
    private String direccion;
    private String nogps;
    private String last;
    private Location punto_mallku;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        buildGoogleApiClient();
        String[]valor;
        setContentView(R.layout.activity_mapas);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        Context contexto =  this.getApplicationContext();

        vibrator=(Vibrator)contexto.getSystemService(VIBRATOR_SERVICE);
        SharedPreferences prefe = getSharedPreferences("Numero", Context.MODE_PRIVATE);
        fecha = prefe.getString("fecha", "0.0");
        completo = prefe.getString("varios", "");
        tipo_alarma=prefe.getString("alerta_tipo","");
        latitud=prefe.getString("latitud","0.00");
        longitud=prefe.getString("longitud","0.00");
        tipo_alerta=prefe.getString("alerta_tipo","");
        Speed=prefe.getString("velocidad","0.0");
        direccion=prefe.getString("url","");
        nogps=prefe.getString("sin_gps","no");
        last=prefe.getString("ultimo:","");

        ImageView power=(ImageView)findViewById(R.id.power);
        power.setVisibility(View.INVISIBLE);
        ImageView puerta=(ImageView)findViewById(R.id.puerta);
        puerta.setVisibility(View.INVISIBLE);
        ImageView ignicion=(ImageView)findViewById(R.id.ignicion);
        ignicion.setVisibility(View.INVISIBLE);
        ImageView alarma=(ImageView) findViewById(R.id.alarma_imagen);
        alarma.setVisibility(View.VISIBLE);
        ImageButton zona=(ImageButton) findViewById(R.id.crear_geozona);
        zona.setVisibility(View.INVISIBLE);
        TextView sin_gps=(TextView) findViewById(R.id.tv_gps);
        TextView alarma_titulo=(TextView) findViewById(R.id.tv_titulo_alarma);
        alarma_titulo.setVisibility(View.VISIBLE);
        if(nogps.equals("si"))
        {
            sin_gps.setVisibility(View.VISIBLE);
        }
        else
        {
            sin_gps.setVisibility((View.INVISIBLE));
        }
        //sin_gps.setVisibility(View.INVISIBLE);
        if(tipo_alarma.equalsIgnoreCase("acc alarm!"))
        {
            alarma.setImageResource(R.drawable.alerta_ignicion_bajo);
            alarma_titulo.setText("Alerta de Ignición");
        }
        if(tipo_alarma.equalsIgnoreCase("acc on!"))
        {
            alarma.setImageResource(R.drawable.alerta_ignicion_bajo);
            alarma_titulo.setText("Alerta de Encendido de Ignición");
        }
        if(tipo_alarma.equalsIgnoreCase("acc off!"))
        {
            alarma.setImageResource(R.drawable.alerta_ignicion_bajo);
            alarma_titulo.setText("Alerta de Apagado de Ignición");
        }
        if(tipo_alarma.equalsIgnoreCase("Power Alarm!")) {
            alarma.setImageResource(R.drawable.alerta_power_bajo);
            alarma_titulo.setText("Alerta de Bateria");
        }
        if(tipo_alarma.equalsIgnoreCase("door Alarm!")) {
            alarma.setImageResource(R.drawable.alerta_puerta_bajo);
            alarma_titulo.setText("Alerta de Puerta");
        }
        if(tipo_alarma.equalsIgnoreCase("Battery Alarm!")) {
            alarma.setImageResource(R.drawable.alerta_bateria_bajo);
            alarma_titulo.setText("Alerta de Bateria del Dispositivo");
        }
        if(tipo_alarma.equalsIgnoreCase("Sensor Alarm!")) {
            alarma.setImageResource(R.drawable.alerta_robo_bajo);
            alarma_titulo.setText("Alerta Antirrobo");
        }
        if(tipo_alarma.equalsIgnoreCase("Help me!")) {
            alarma.setImageResource(R.drawable.alerta_panic_bajo);
            alarma_titulo.setText("Alerta de Botón de Panico");
        }
        if(tipo_alarma.equalsIgnoreCase("Move!")) {
            alarma.setImageResource(R.drawable.alerta_movilidad_bajo);
            alarma_titulo.setText("Alerta de Movimiento");
        }
        if(tipo_alarma.equalsIgnoreCase("Speed!")) {
            alarma.setImageResource(R.drawable.alerta_velocidad_bajo);
            alarma_titulo.setText("Alerta de Exceso de Velocidad");
        }
        if(tipo_alarma.equalsIgnoreCase("oil")) {
            alarma.setImageResource(R.drawable.alerta_combustible_bajo);
            alarma_titulo.setText("Alerta de Combustible");
        }
        if(tipo_alarma.equalsIgnoreCase("No gps!")) {
            alarma.setImageResource(R.drawable.alerta_no_gps_bajo);
            alarma_titulo.setText("Alerta de Perdida de Señal GPS");
            sin_gps.setVisibility(View.VISIBLE);
        }
        if(tipo_alarma.equalsIgnoreCase("Low Battery!")) {
            alarma.setImageResource(R.drawable.alerta_bateria_bajo);
            alarma_titulo.setText("Alerta de Bateria Baja");
        }
        if(tipo_alarma.equalsIgnoreCase("stockade!")) {
            alarma.setImageResource(R.drawable.alerta_geozonacuadrada_bajo);
            alarma_titulo.setText("Alerta de Geozona Rectangular");
        }
        if(tipo_alarma.equalsIgnoreCase("PowerAlarm")) {
            alarma.setImageResource(R.drawable.alerta_power_bajo);
            alarma_titulo.setText("Alerta de bateria");
        }
            ubicacion_dispositivo= new LatLng(Double.parseDouble(latitud),Double.parseDouble(longitud));
            milatitud=Double.parseDouble(latitud);
            milongitud=Double.parseDouble(longitud);


        mapFragment.getMapAsync(this);
        mapFragment.getView().setClickable(false);
    }

    public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = null;
        mMap = googleMap;
        CuantoZoom= 16;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
            }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                Log.d("Map", "Map clicked");
            }

        });
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if(marker.getTitle().equals("Mallku Cars"))
                {
                    String acomodado;
                    String encabezado = null;
                    milatitud=marker.getPosition().latitude;
                    milongitud=marker.getPosition().longitude;
                    fecha=fecha.replace("T:","");
                    String todo;


                        String[] fecha_hora = fecha.split(" ");
                        String[] fecha_espanol = fecha_hora[0].split("/");
                        try {
                            encabezado="Enviado desde Mallku Cars :\n"+ fecha_hora[1] + ", " + fecha_espanol[2] + "/" + fecha_espanol[1] + "/" + fecha_espanol[0];
                        }
                        catch (Exception e)
                        {

                            acomodado="--/--/---- 00:00";
                        }

                    if(nogps.equals("no")) {
                        encabezado="Enviado desde Mallku Cars :\n"+ fecha_hora[1] + ", " + fecha_espanol[2] + "/" + fecha_espanol[1] + "/" + fecha_espanol[0];
                        todo =  encabezado+"\n Lat.:" + marker.getPosition().latitude + "\n Long.:" + marker.getPosition().longitude;
                    }
                    else {
                        encabezado="Enviado desde Mallku Cars :\n"+ fecha_hora[1] + ", " + fecha_espanol[2] + "/" + fecha_espanol[1] + "/" + fecha_espanol[0];
                        acomodado="Última Conexion GPS:"+last;
                        todo =  encabezado+"\n"+acomodado + "\n Lat.:" + marker.getPosition().latitude + "\n Long.:" + marker.getPosition().longitude;
                    }
                   show_alert(todo);
                }
                return false;
            }
        });
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            public void onMapLoaded() {
                //do stuff here
                set_distancia();
            }
        });

            //latLng = cargar_puntos_mapa('E');

            Double lati=Double.parseDouble(latitud);
            Double longi=Double.parseDouble(longitud);
        latLng=new LatLng(lati,longi);
        Toast.makeText(getBaseContext(), "Punto "+latLng,
                Toast.LENGTH_LONG).show();
        if (latLng != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, CuantoZoom);
            mMap.animateCamera(cameraUpdate);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

        } else {
            //Button auxiliar = (Button) (findViewById(R.id.config_bt_zona));
            //auxiliar.setEnabled(false);
           // Button auxilia2 = (Button) (findViewById(R.id.distancia));
            //auxilia2.setEnabled(false);

            latLng = new LatLng(lati, longi);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, CuantoZoom);
            mMap.animateCamera(cameraUpdate);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
        }

    }

    private void show_alert(String todo) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("INFORMACIÓN");
        alert.setMessage(todo);
        alert.setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        alert.show();
    }

    public void ver_mas(View view)
    {
        set_distancia();
    }
    public void set_distancia()
    {
        TextView aux_velocidad=(TextView) findViewById(R.id.tv_velocidad);
        TextView aux=(TextView)findViewById(R.id.tv_distancia);
        Speed.replace("speed","Velocidad");
        aux.setText(calcular_distancia());
        aux_velocidad.setText(Speed);
    }
    



    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(60 * 1000);
        mLocationRequest.setFastestInterval(15 * 1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mGoogleApiClient.connect();
    }
    {}
    public void hacer_zoom(View view) {


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocationUpdate();


    }

    private void requestLocationUpdate() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        else {
            try {
                CuantoZoom = mMap.getCameraPosition().zoom;
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                mMap.setMyLocationEnabled(true);
            }
            catch (Exception e){

            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onLocationChanged(Location location) {
        mMap.clear();
        mudar_marcador(milatitud,milongitud);

        milatitud=location.getLatitude();
        milongitud=location.getLongitude();
        mMap.moveCamera(CameraUpdateFactory.zoomBy(CuantoZoom));
        mMap.setMyLocationEnabled(true);
    }
    private void mudar_marcador(double milatitud, double milongitud) {

        LatLng ubicacion_evento = new LatLng(Double.parseDouble(latitud), Double.parseDouble(longitud));
        ubicacion_dispositivo=new LatLng(milatitud,milongitud);

        Bitmap bmp = BitmapFactory.decodeResource(
                getBaseContext().getResources(), R.drawable.localizacion_telf);
        Bitmap bmp_evento = BitmapFactory.decodeResource(
                getBaseContext().getResources(), R.drawable.localizacion_mallku);
        Bitmap scaled = Bitmap.createScaledBitmap(bmp, 60, 80, false);
        Bitmap scaled1 = Bitmap.createScaledBitmap(bmp_evento, 60, 80, false);
        mCurrLocationMarker=mMap.addMarker(new MarkerOptions()
                .title("Mallku Cars")
                .draggable(false)
                .position(ubicacion_evento)
                .icon(BitmapDescriptorFactory.fromBitmap(scaled1)));
        mCurrLocationMarker=mMap.addMarker(new MarkerOptions()
                .title("Telefono")
                .draggable(false)
                .position(ubicacion_dispositivo)
                .icon(BitmapDescriptorFactory.fromBitmap(scaled)));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ubicacion_dispositivo, CuantoZoom));
    }

    public void unidad_medida(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.kilometros:
                if (checked)
                    unidad_medida="K";
                    break;
            case R.id.metros:
                if (checked)
                    unidad_medida="M";
                    break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    public  void enviar(String mensaje) {
        PendingIntent piSent = PendingIntent.getBroadcast(getApplicationContext(),
                0, new Intent(SENT), 0);
        PendingIntent piDelivered = PendingIntent.getBroadcast(getApplicationContext(),
                0, new Intent(DELIVERED), 0);
        final BroadcastReceiver myReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
                unregisterReceiver(this);
            }
        };
        registerReceiver(myReceiver, new IntentFilter(SENT));
        //unregisterReceiver(myReceiver );

        //---when the SMS has been delivered---
        final BroadcastReceiver myReceiverDev = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
                unregisterReceiver(this);
            }
        };

        registerReceiver(myReceiverDev, new IntentFilter(DELIVERED));
        unregisterReceiver(myReceiverDev );
        SharedPreferences prefe = getSharedPreferences("Numero", Context.MODE_PRIVATE);
        numero_disp = prefe.getString("num", "nada");
        clave = prefe.getString("clave", "0000");


        String phoneNo = numero_disp;
        String sms = mensaje;

        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, sms, piSent, piDelivered);
            //smsManager.sendTextMessage(phoneNo, null, sms, null, null);
            /*Toast.makeText(getApplicationContext(), "SMS Sent!",
                    Toast.LENGTH_LONG).show();*/
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),
                    "" + e.getMessage(),
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
    public String calcular_distancia()
    {
        double distancia=0.0;



        //Location ultimo_punto=buscar_ultimo_base_datos(true);
        Location celular=null;
        celular=busco_celular();
       /* Toast.makeText(getApplicationContext(), "El celular"+celular.toString(),
                Toast.LENGTH_LONG).show();*/

        if(celular!=null&&punto_mallku!=null)
        {
            distancia=hacer_calculo_distancia(celular,punto_mallku);
            pintar_ruta(celular,punto_mallku);
            DecimalFormat df=new DecimalFormat("0.00");
            String formate = df.format(distancia);
        }
        DecimalFormat df=new DecimalFormat("0.00");
        if(distancia>=1000)
        { String formate = df.format(distancia/1000);
            return  formate+" Km";}
        else
            return df.format(distancia)+"m";
    }

    private void pintar_ruta(Location celular, Location ultimo_punto) {

    }

    public void calcular_distancia(View view)
    {

    if(view.getId()==R.id.distancia)
    {
        /*Location centro_zona=null;
        Location ultimo_punto=null;
        double distancia;
        centro_zona=buscar_ultimo_base_datos('Z');*/
        Location ultimo_punto=buscar_ultimo_base_datos(true);
        Location celular=null;
        celular=busco_celular();
       /* Toast.makeText(getApplicationContext(), "El celular"+celular.toString(),
                Toast.LENGTH_LONG).show();*/

        if(celular!=null&&ultimo_punto!=null)
        {

            double distancia=hacer_calculo_distancia(celular,ultimo_punto);
            DecimalFormat df=new DecimalFormat("0.00");
            String formate = df.format(distancia);
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("La distancia al dispositivo: "+formate+" mts.");
            alert.setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });
            alert.show();

            if (distancia>tam_zona)
            {
                long[] pattern ={0,500,110};

                if(vibrator.hasVibrator())
                    vibrator.vibrate(pattern,-1);
                else {
                    Toast.makeText(getApplicationContext(), "Este telefono no puede vibrar..."+distancia+"mts",
                            Toast.LENGTH_LONG).show();
                }

            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "No se tienen los puntos del centro de Zona y la ultima posición",
                    Toast.LENGTH_LONG).show();
        }
    }
    }

    private Location busco_celular() {
       // Log.i("mapa",""+mMap.getMyLocation().toString());
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Location myLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (myLocation == null) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_COARSE);
            String provider = lm.getBestProvider(criteria, true);
            myLocation = lm.getLastKnownLocation(provider);
        }
        return mMap.getMyLocation();
    }

    private float hacer_calculo_distancia(Location centro_zona, Location ultimo_punto) {
        float retorno=centro_zona.distanceTo(ultimo_punto);
        return retorno;
    }

    private Location buscar_ultimo_base_datos(boolean algo) {
        Cursor pun;
        MessagesDb todos=new MessagesDb(getBaseContext());
        Location devolver=new Location("");
//        String fecha = new SimpleDateFormat("YY-MM-dd").format(new Date());
        pun= todos.datos_puntos('M',1);
        if(pun!=null) {
            pun.moveToFirst();
            int donde;
            donde = pun.getColumnIndex("latitud");
            try {
                double latitud = pun.getDouble(donde);
                donde = pun.getColumnIndex("longitud");
                double longitud = pun.getDouble(donde);
                devolver.setLatitude(latitud);
                devolver.setLongitude(longitud);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        return  devolver;
    }
    private Location buscar_ultimo_base_datos(char que) {
        Cursor pun;
        MessagesDb todos=new MessagesDb(getBaseContext());
        Location devolver=new Location("");
        String fecha = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        pun= todos.datos_puntos(que,1);
        if(pun!=null)
        {  pun.moveToFirst();
            do{
                int donde;
                donde=pun.getColumnIndex("latitud");
           double latitud=pun.getDouble(donde);
                donde=pun.getColumnIndex("longitud");
           double longitud=pun.getDouble(donde);
                Toast.makeText(getApplicationContext(), "Cual punto:"+que+" latitud "+latitud+" longitud "+longitud,
                        Toast.LENGTH_LONG).show();
                if(que=='Z')
                {
                    donde=pun.getColumnIndex("radio");
                    if(donde!=-1)
                    tam_zona=pun.getDouble(donde);
                    else
                        tam_zona=0;
                }
           devolver.setLatitude(latitud);
           devolver.setLongitude(longitud);
           return devolver;
            }while(pun.moveToNext());
        }
        else
            return null;
    }
    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mGoogleApiClient.isConnected())
        {
            requestLocationUpdate();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

}
